cmake_minimum_required(VERSION 3.15)
project(fft)

option(BUILD_SHARED_LIBS "Build using shared libraries" ON)

find_package(yat CONFIG REQUIRED)
find_package(utils CONFIG REQUIRED)
find_package(fftw CONFIG REQUIRED)

add_compile_definitions(
    PROJECT_NAME=${PROJECT_NAME}
    PROJECT_VERSION=${PROJECT_VERSION}
)

file(GLOB_RECURSE sources
    src/*.cpp
)

set(includedirs 
    src
    include
)

add_library(fft ${sources})
target_include_directories(fft PRIVATE ${includedirs})
target_link_libraries(fft PRIVATE yat::yat)
target_link_libraries(fft PRIVATE utils::utils)
target_link_libraries(fft PRIVATE fftw::fftw)

if (MAJOR_VERSION)
    set_target_properties(fft PROPERTIES VERSION ${PROJECT_VERSION} SOVERSION ${MAJOR_VERSION})
endif()

install(DIRECTORY ${CMAKE_SOURCE_DIR}/include/ DESTINATION include
    FILES_MATCHING PATTERN "*.h" PATTERN "*.hpp"
)

install(TARGETS fft LIBRARY DESTINATION ${LIB_INSTALL_DIR})
