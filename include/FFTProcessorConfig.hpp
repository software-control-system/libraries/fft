#ifndef INCLUDE_FFTPROCESSORCONFIG_HPP_HEADER_INCLUDED_BC2A0469
#define INCLUDE_FFTPROCESSORCONFIG_HPP_HEADER_INCLUDED_BC2A0469

#include "Window.hpp"

#include "ValueOutOfBoundException.h"
#include "DivideByZeroException.h"
#include "NullPointerException.h"

namespace FFT
{
/**
 *  \brief Handles the configuration of the FFTProcessor class
 */

//##ModelId=43C7DB830035
class FFTProcessorConfig
{
  public:
    //##ModelId=43D9F5FC0339
    FFTProcessorConfig();

    //##ModelId=43D9F603009A
    ~FFTProcessorConfig();

    //##ModelId=43CD132A021F
    void init(double samplingFrequency, double desiredRBW, WindowType windowType, double overlapPercent, size_t minimalSpectralLines) throw(ValueOutOfBoundException);

    //##ModelId=43CD132A023E
    double getResolution() const;

    //##ModelId=43D1105A03AC
    double getSamplingFrequency() const;

    //##ModelId=43CD132A0240
    size_t getWindowSize() const;

    //##ModelId=43CF6AD801B3
    double getOverlapPercent() const;

    //##ModelId=43D1109A009B
    const Window& getWindow() const throw(NullPointerException);
    //##ModelId=43FD9935003B
    size_t getFFTSize() const;



  private:
    //##ModelId=43C7DBCB023C
    double _samplingFrequency; ///< the sampling frequency \f$ f_s \f$

    //##ModelId=43C7DBCB023D
    double _resolutionBandWidth; ///< the effective resolution bandwidth \f$ df \f$

    //##ModelId=43C7DBCB024C
    Window* _window; ///< the window associated with the config

    //##ModelId=43C7DBCB025B
    size_t _windowSize; ///< the number of input samples \f$ N \f$

    //##ModelId=43FD9891017C
    size_t _fftSize; ///< the number of points on which the FFT is computed

    //##ModelId=43CF68780268
    double _overlapPercent; ///< the percentage of overlap between 2 consecutive FFT computations


};

}

#endif /* INCLUDE_FFTPROCESSORCONFIG_HPP_HEADER_INCLUDED_BC2A0469 */
