#ifndef INCLUDE_SPECTRUM_HPP_HEADER_INCLUDED_BC03812B
#define INCLUDE_SPECTRUM_HPP_HEADER_INCLUDED_BC03812B

#include "NoSuchElementException.h"
#include "WindowInfo.hpp"

#if defined(_WIN32) || defined(WIN32)
#if defined(_MSC_VER)
// disable warning C4275: non dll-interface class 'std::_Complex_base<float>' used as base for dll-interface class 'std::complex<float>' 
#pragma warning ( disable : 4275 )
#endif
#endif

#include <complex>

#if defined(_WIN32) || defined(WIN32)
#if defined(_MSC_VER)
#pragma warning ( default : 4275 )
#endif
#endif

namespace FFT {


/**
 *  \brief The type of data represented by a DataBlock
 */
typedef enum
{
  REAL, ///< real data type
  COMPLEX_INTERLEAVED ///< complex data type compatible with std::complex<double>
} ScalarType;


/**
 *  \brief Encapsulate spectrum data and corresponding information on how it was generated
 */
//##ModelId=43FC3F9F00CE
class Spectrum
{
  public:
    /**
     *  \brief Constructor
     *
     *  \param[in] type complex or real spectrum
     *  \param[in] size the number of samples
     *  \param[in] windowInfo contains info about the window used to create this spectrum
     *  \param[in] fs the sampling frequency
     *  \param[in] df the frequency step between 2 consecutive bins
     *  \param[in] fftSize the number of points on which the fft has been computed
     */
    //##ModelId=43FC3FB802F2
    Spectrum(ScalarType type, size_t size, const WindowInfo& windowInfo, double fs, double df, size_t fftSize);

    /**
     *  \brief Destructor
     */
    //##ModelId=43FC3FB802F4
    ~Spectrum();

    /**
     *  \brief Returns the type of data contained in the spectrum
     */
    //##ModelId=43FC3FB80303
    ScalarType getType() const;

    /**
     *  \brief Return the real data of a specified block
     * 
     *  \exception NoSuchElementException if the selected block is not of type REAL
     *  \return the pointer to the real data contained in a block
     */
    //##ModelId=43FC3FB80321
    const double* getRealData() const throw(NoSuchElementException);

    /**
     *  \brief Return the real data of a specified block
     * 
     *  \exception NoSuchElementException if the selected block is not of type REAL
     *  \return the pointer to the real data contained in a block
     */
    //##ModelId=43FC3FB80325
    double* getRealData() throw(NoSuchElementException);

    /**
     *  \brief Return the complex data of a specified block
     * 
     *  \exception NoSuchElementException if the selected block is not of type REAL
     *  \return the pointer to the complex data contained in a block
     */
    //##ModelId=43FC3FB80323
    const std::complex<double>* getComplexData() const throw(NoSuchElementException);

    /**
     *  \brief Return the complex data of a specified block
     * 
     *  \exception NoSuchElementException if the selected block is not of type REAL
     *  \return the pointer to the complex data contained in a block
     */
    //##ModelId=43FC3FB80331
    std::complex<double>* getComplexData() throw(NoSuchElementException);

    /**
     *  \brief Return the number of bins managed by this instance
     */
    //##ModelId=43FC3FB80302
    size_t getNbBins() const;

    /**
     *  \brief Return the sampling frequency
     */
    //##ModelId=43FC41260322
    double getSamplingFrequency() const;

    /**
     *  \brief Return the frequency step betwwen 2 consecutive bins
     */
    //##ModelId=43FC4144034E
    double getResolution() const;

    /**
     *  \brief Return the window informations
     */
    //##ModelId=43FC414F02A8
    const WindowInfo& getWindowInfo() const;
    
    /**
     *  \brief Return the window size (number of input samples used to generate the spectrum)
     */
    //##ModelId=43FDBDD0030A
    size_t getWindowSize() const;

    /**
     *  \brief Return the FFT size (number of bins on which FFT has been computed)
     */
    //##ModelId=43FDBDE302AA
    size_t getFFTSize() const;

  private:
    //##ModelId=43FC40280030
    ScalarType _type; ///< complex or real spectrum

    //##ModelId=43FC401B010A
    void* _data; ///< the actual data of the spectrum

    //##ModelId=43FC402102EF
    size_t _size; ///< the number of frequency bins

    //##ModelId=43FC40320097
    double _fs; ///< the sampling frequency

    //##ModelId=43FC407802AC
    double _df; ///< the frequency step between 2 consecutive bins

    //##ModelId=43FC403702D1
    WindowInfo _windowInfo; ///< contains info about the window used to create this spectrum

    //##ModelId=43FDBE6800C7
    size_t _fftSize; ///< the number of points on which the fft has been computed (should be #_size / 2 + 1 in most cases)


};

}

#endif /* INCLUDE_SPECTRUM_HPP_HEADER_INCLUDED_BC03812B */
