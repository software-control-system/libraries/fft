#ifndef INCLUDE_VECTORAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1DAE7C
#define INCLUDE_VECTORAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1DAE7C

#if defined(_WIN32) || defined(WIN32)
#if defined(_MSC_VER)
// disable warning C4275: non dll-interface class 'std::_Complex_base<float>' used as base for dll-interface class 'std::complex<float>' 
// disable warning C4786: 'identifier' : identifier was truncated to 'number' characters in the debug information
#pragma warning ( disable : 4275 4786 )
#endif
#endif

#include <complex>
#include "AverageProcessor.h"
#include "AveragedSpectrumProcessorBase.hpp"

#if defined(_WIN32) || defined(WIN32)
#if defined(_MSC_VER)
#pragma warning ( default : 4275 4786 )
#endif
#endif


namespace FFT
{
/**
 *  \brief Base class for all Vector-based averaged spectrum processor
 *
 *  Vector means here that the averaging is done on complex data.
 *  
 *  Be aware that averaging on complex data make use of phase information,
 *  so the input signal must be triggered for Vector averaging to work properly.
 *  You should consider using RMS averaging if your input signal is not triggered
 *
 *  Its specificity is to maintain an AverageProcessor<std::complex<double>>
 */
//##ModelId=43DF2B1201D0
class VectorAveragedSpectrumProcessor : public AveragedSpectrumProcessorBase
{
  public:

    /**
     *  \brief Constructor
     *
     *  \param[in] method the kind of temporal averaging (one shot, moving average...)
     *  \sa TimeAveragingMethod
     */
    //##ModelId=43E202A701D0
    VectorAveragedSpectrumProcessor(TimeAveragingMethod method) throw(IndexOutOfBoundException);

    /**
     *  \brief Destructor
     */
    //##ModelId=43E202AB01FF
    virtual ~VectorAveragedSpectrumProcessor();

    //##ModelId=43E202D103A6
    size_t getNbMissingSpectrum() const;

    //##ModelId=43E202D103B6
    void restart();

  protected:
    //##ModelId=43EA0B3B0070
    typedef std::complex<double> complex_double;
    //##ModelId=43E202D103B7
    void initAverageProcessor();
    //##ModelId=43DF2B3D008B
    AverageProcessor<complex_double>* _processor;

};
}


#endif /* INCLUDE_VECTORAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1DAE7C */
