#ifndef INCLUDE_PEAKHOLDPOWERAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1DF25E
#define INCLUDE_PEAKHOLDPOWERAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1DF25E
#include "PeakHoldAveragedSpectrumProcessor.hpp"
class Spectrum;

namespace FFT
{
/**
 *  \brief Implementation of the Peak Hold Power Averaging
 *
 *  Peak Hold Power Averaging retains the RMS peak levels of the averaged quantities.
 *  It performs peak-hold at each frequency bin separately to retain peak RMS levels
 *  from one FFT frame to the next.
 *
 *  Let \f$ X_{k} \f$ be the k-th input complex spectrum.
 *  The formula used to compute the averaged spectrum is:
 *  \f[
 *     PeakHoldAverage = Max_{k}\left( | X_k |^2 \right) = Max_{k}\left( X_k  X_k^* \right)
 *  \f]
 */
//##ModelId=43DE46D601BF
class PeakHoldPowerAveragedSpectrumProcessor : public PeakHoldAveragedSpectrumProcessor
{
  public:
    /**
     *  \brief Constructor
     *
     *  \param[in] method the kind of temporal averaging (one shot, moving average...)
     *  \sa TimeAveragingMethod
     */
    //##ModelId=43E233E60395
    PeakHoldPowerAveragedSpectrumProcessor(TimeAveragingMethod method);

    /**
     *  \brief Destructor
     */
    //##ModelId=43E233E60397
    virtual ~PeakHoldPowerAveragedSpectrumProcessor();

    //##ModelId=43E233E603A5
    bool process(const Spectrum& fftSpectrumData);

};
}


#endif /* INCLUDE_PEAKHOLDPOWERAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1DF25E */
