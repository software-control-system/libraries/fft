#ifndef INCLUDE_PEAKHOLDAVERAGING_HPP_HEADER_INCLUDED_BC1D8B4A
#define INCLUDE_PEAKHOLDAVERAGING_HPP_HEADER_INCLUDED_BC1D8B4A

#include "AverageProcessor.h"

namespace FFT
{

//##ModelId=43DF2B6003CB
class PeakHoldAveraging : public DefaultAveraging<double>
{
  public:
    //##ModelId=43E23464021C
    PeakHoldAveraging();

    //##ModelId=43E234660162
    virtual ~PeakHoldAveraging();

    //##ModelId=43DF2B9101BB
    const double* process(const double* newData, double weight, double lastDataWeight);

};
}


#endif /* INCLUDE_PEAKHOLDAVERAGING_HPP_HEADER_INCLUDED_BC1D8B4A */
