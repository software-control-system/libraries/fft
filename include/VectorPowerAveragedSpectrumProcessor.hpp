#ifndef INCLUDE_VECTORPOWERAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1D802D
#define INCLUDE_VECTORPOWERAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1D802D
#include "VectorAveragedSpectrumProcessor.hpp"
class Spectrum;

namespace FFT
{

class Data;

/**
 *  \brief Implementation of the Vector Power Averaging
 *
 *  Vector Power Averaging means here that the averaging is done on complex data
 *  and that the RMS Value is taken from the complex averaged
 *
 *  Let \f$ X_{k} \f$ be the k-th input complex spectrum.
 *  The formula used to compute the averaged spectrum is:
 *  \f[
 *     VectorAv = \left| Mean_k\left( X_k \right) \right|^2 = Mean_k\left( X_k \right) Mean_k\left( X_k \right)^*
 *  \f]
 */
//##ModelId=43DE46D000F4
class VectorPowerAveragedSpectrumProcessor : public VectorAveragedSpectrumProcessor
{
  public:
    /**
     *  \brief Constructor
     *
     *  \param[in] method the kind of temporal averaging (one shot, moving average...)
     *  \sa TimeAveragingMethod
     */
    //##ModelId=43E1DBD8035B
    VectorPowerAveragedSpectrumProcessor(TimeAveragingMethod method);

    /**
     *  \brief Destructor
     */
    //##ModelId=43E1DBD8036A
    virtual ~VectorPowerAveragedSpectrumProcessor();

    //##ModelId=43E1DBD8036B
    bool process(const Spectrum& fftSpectrumData);

};

}

#endif /* INCLUDE_VECTORPOWERAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1D802D */
