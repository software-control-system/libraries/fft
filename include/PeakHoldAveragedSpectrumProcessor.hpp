#ifndef INCLUDE_PEAKHOLDAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1DD13F
#define INCLUDE_PEAKHOLDAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1DD13F

#include "AverageProcessor.h"
#include "PeakHoldAveraging.hpp"
#include "AveragedSpectrumProcessorBase.hpp"

namespace FFT
{
/**
 *  \brief Base class for all PeakHold-based averaged spectrum processor
 *
 *  Peak Hold averaging retains the RMS peak levels of the averaged quantities.
 *  It performs peak-hold at each frequency bin separately to retain peak RMS levels
 *  from one FFT frame to the next.
 *
 *  Its specificity is to maintain an AverageProcessor<double,double,double,PeakHoldAveraging>
 */
//##ModelId=43DE49930054
class PeakHoldAveragedSpectrumProcessor : public AveragedSpectrumProcessorBase
{
  public:
    /**
     *  \brief Constructor
     *
     *  \param[in] method the kind of temporal averaging (one shot, moving average...)
     *  \sa TimeAveragingMethod
     */
    //##ModelId=43E232D0026F
    PeakHoldAveragedSpectrumProcessor(TimeAveragingMethod method) throw(IndexOutOfBoundException);

    /**
     *  \brief Destructor
     */
    //##ModelId=43E232D0028E
    virtual ~PeakHoldAveragedSpectrumProcessor();

    //##ModelId=43E232D0028F
    size_t getNbMissingSpectrum() const;

    //##ModelId=43E232D0029E
    void restart();

  protected:
    //##ModelId=43E232D002AD
    void initAverageProcessor();


    //##ModelId=43E232B6028E
    AverageProcessor<double, double, double, PeakHoldAveraging>* _processor;

};

}

#endif /* INCLUDE_PEAKHOLDAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1DD13F */
