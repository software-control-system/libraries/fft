#ifndef INCLUDE_PHASEPROCESSOR_HPP_HEADER_INCLUDED_BC163956
#define INCLUDE_PHASEPROCESSOR_HPP_HEADER_INCLUDED_BC163956

#include "SpectrumProcessorBase.hpp"


namespace FFT
{

/**
 *  \brief Possible units for the phase
 */
typedef enum
{
  RADIANS,
  DEGREES
} PhaseUnits;



/**
 *  \brief Extracts phase information from a spectrum, and optionally unwraps it.
 */
//##ModelId=43CE520802D4
class PhaseProcessor : public SpectrumProcessorBase
{
  public:
    /**
     *  \brief Constructor
     */
    //##ModelId=43E9F3C103AB
    PhaseProcessor();

    /**
     *  \brief Destructor
     */
    //##ModelId=43E9F3C103BA
    virtual ~PhaseProcessor();

    //##ModelId=43E9F34500BE
    bool process(const Spectrum& fftSpectrumData);

    /**
     *  \brief Initializes the instance
     * 
     *  It is possible to unwrap phase values to produce smoother phase plots
     *  Look at unWrap() documentation for deeper explanation on this process
     *
     *  \param[in] units the units in which the output will be computed
     *  \param[in] unWrap boolean indicating if unwrapping must take place
     *  \sa unWrap() for explanation on unwrapping process
     */
    //##ModelId=43E9F35203AF
    void init(PhaseUnits units, bool unWrap);

  private:
    /**
     *  \brief Unwrap the phase value to produce smoother phase plots
     * 
     *  The method corrects the radian phase angles by adding
     *  multiples of \f$2\pi\f$ when absolute jumps between consecutive elements of the phase
     *  are greater than the default tolerance of \f$\pi\f$
     *
     *  \param[in] phaseValue a pointer to phase values to be unwrapped
     *  \param[in] n the number of elements pointed by the buffer
     */
    //##ModelId=43E9F5BE03CC
    void unWrap(double* phaseValue, size_t n);

    //##ModelId=43E9F3820242
    PhaseUnits _units; ///< units in which the output is computed

    //##ModelId=43E9F388029F
    bool _unWrap; ///< indicates if unwrapping must be done

};

}

#endif /* INCLUDE_PHASEPROCESSOR_HPP_HEADER_INCLUDED_BC163956 */
