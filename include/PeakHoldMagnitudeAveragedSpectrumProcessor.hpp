#ifndef INCLUDE_PEAKHOLDMAGNITUDEAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1DE490
#define INCLUDE_PEAKHOLDMAGNITUDEAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1DE490
#include "PeakHoldAveragedSpectrumProcessor.hpp"
class Spectrum;

namespace FFT
{

/**
 *  \brief Implementation of the Peak Hold Magnitude Averaging
 *
 *  Peak Hold Magnitude Averaging retains the RMS peak levels of the averaged quantities.
 *  It performs peak-hold at each frequency bin separately to retain peak RMS levels
 *  from one FFT frame to the next.
 *
 *  Let \f$ X_{k} \f$ be the k-th input complex spectrum.
 *  The formula used to compute the averaged spectrum is:
 *  \f[
 *     PeakHoldAverage = Max_k\left( | X_k | \right) = Max_k\left( \sqrt{X_k  X_k^*} \right)
 *  \f]
 */
//##ModelId=43DE46BD01DE
class PeakHoldMagnitudeAveragedSpectrumProcessor : public PeakHoldAveragedSpectrumProcessor
{
  public:
    /**
     *  \brief Constructor
     *
     *  \param[in] method the kind of temporal averaging (one shot, moving average...)
     *  \sa TimeAveragingMethod
     */
    //##ModelId=43E233C60064
    PeakHoldMagnitudeAveragedSpectrumProcessor(TimeAveragingMethod method);

    /**
     *  \brief Destructor
     */
    //##ModelId=43E233C60074
    virtual ~PeakHoldMagnitudeAveragedSpectrumProcessor();

    //##ModelId=43E233C60075
    bool process(const Spectrum& fftSpectrumData);

};

}

#endif /* INCLUDE_PEAKHOLDMAGNITUDEAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1DE490 */
