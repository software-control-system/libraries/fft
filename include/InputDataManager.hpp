#ifndef INCLUDE_INPUTDATAMANAGER_HPP_HEADER_INCLUDED_BC29B5E7
#define INCLUDE_INPUTDATAMANAGER_HPP_HEADER_INCLUDED_BC29B5E7

#include "NullPointerException.h"

namespace FFT
{
/**
 *  \brief Stores the incoming samples for the FFTProcessor class,
 *         concatenates them in a single buffer,
 *         and handles the overlaping region between two consecutive computations
 */

//##ModelId=43C7AED30151
class InputDataManager
{
  public:
    //##ModelId=43D8F68D01E8
    InputDataManager();

    //##ModelId=43D8FB9803D7
    ~InputDataManager();

    //##ModelId=43C7AF6503C7
    bool sendInput(const double* inputData, size_t n) throw(NullPointerException);

    //##ModelId=43C7AFBE02E9
    const double* getInputBlock() const throw(NullPointerException);

    //##ModelId=43CB6D6A03CE
    void reset();

    //##ModelId=43CE5D05002D
    void init(size_t blockSize, double overlapPercent);

    //##ModelId=43CF6C5801B9
    size_t getNbMissingSamples() const;

    //##ModelId=43CF6D93008A
    void beginNextBlockAquisition() throw(NullPointerException);
    
    //##ModelId=43DE0F3D03B9
    void restart();


  private:
    //##ModelId=43C7AEDE01DD
    double* _inputBlock; ///< the buffer holding the incoming samples

    //##ModelId=43C7AEE50378
    size_t _blockSize; ///< the size of #_inputBlock

    //##ModelId=43CF686302E8
    size_t _overlapingSamples; ///< the number of overlaping samples

    //##ModelId=43C7AEF1001C
    size_t _cursor; ///< the current position inside the #_inputBlock buffer

};

}

#endif /* INCLUDE_INPUTDATAMANAGER_HPP_HEADER_INCLUDED_BC29B5E7 */
