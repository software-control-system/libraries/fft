#ifndef AVERAGEPROCESSOR_H_HEADER_INCLUDED_BC276C6A
#define AVERAGEPROCESSOR_H_HEADER_INCLUDED_BC276C6A

#include <deque>
#include <string.h>

/**
 *  \brief Implements a default averaging algorithm
 *
 *  This class is used as a basic functionnality of the AverageProcessor class.
 *
 *  It implements the following function through the process method:
 *  \f{eqnarray*} Y_{0} &=& X_{0} \\ Y_{k+1} &=& \alpha Y_{k} + \beta X_{k+1}\f}
 *  where :
 *    - \f$X_{k}\f$ is a vector of type T
 *    - \f$Y_{k}\f$ is a vector of type U
 *    - \f$\alpha\f$ and \f$\beta\f$ are of type W
 *
 *  Consequently, the following must be satified :
 *    - class W must implement : operator * (T&)
 *    - the resulting type of this operator must implement the '+' operator
 *    - the result of the '+' operator must be 'castable' to type U
 *
 *  This gives the user a generic averaging function that can be instantiated
 *  for a wide range of basic types, for example T = int, U = double, W = float.
 *   
 *  This class can be derived to implement other 'averaging-like' function.
 *  For example you can implement a peak-hold averaging by by deriving DefaultAveraging
 *  and re-implementing the process() method to implement \f{eqnarray*} Y_{k+1} &=& max( Y_{k}, X_{k+1} )\\ Y_{0} &=& X_{0}\f}
 *
 */
template <typename T, typename U = T, typename W = double>
class DefaultAveraging
{
  public:
    /**
     *  \brief Constructor
     */
    DefaultAveraging() {this->_nbSamples = 0; this->_last = 0L;};

    /**
     *  \brief Destructor
     */
    virtual ~DefaultAveraging()
    {
      if (this->_last)
        delete [] this->_last;
      this->_nbSamples = 0;
    };

    /**
     *  \brief Implements an averaging function
     *
     *  Given a vector of input samples, and 2 weights \f$\alpha\f$ and \f$\beta\f$,
     *  it realizes :
     *  \f{eqnarray*} Y_{0} &=& X_{0} \\ Y_{k+1} &=& \alpha Y_{k} + \beta X_{k+1}\f}
     *
     */
    virtual const U* process(const T* newData, W weight, W historyWeight)
    {
      for (size_t i = 0; i < this->_nbSamples; i++)
        this->_last[i] = weight * newData[i] + historyWeight * this->_last[i];

      return((const U*)_last);
    };

    /**
     *  \brief Reset the instance
     *
     *  This method puts the instance in a state similar to the one it was just after creation.
     */
    virtual void reset()
    {
      for (size_t i = 0; i < this->_nbSamples; i++)
        this->_last[i] = 0;
    };

    /**
     *  \brief Initialize the instance with the vector size and allocate memory
     */
    virtual void init(size_t nbSamples)
    {
      this->_nbSamples = nbSamples;
      if (this->_last)
        delete [] this->_last;
      this->_last = new U [this->_nbSamples];
      ::memset(this->_last, 0, this->_nbSamples * sizeof(U));
    };

  protected:
    U* _last; ///< the buffer that is given in output. it holds \f$Y_{k}\f$
    size_t _nbSamples; ///< the number of samples in the \f$X_{k}\f$ and \f$Y_{k}\f$ buffer
};







/**
 *  \brief Base class for the different temporal averaging method
 *
 *  This class processes buffers (vectors) of a template input type, and gives back (when ready)
 *  a buffer of a template output type of the same dimension as the input.
 *
 *  Evaluating if output data is ready is a mechanism that is implemented in all derived classes.
 *  All averaging processors have different time-related behavior that will give output data
 *  to the user considering the behavior (moving average, auto restart...) and a 'time constant'
 *  that is an integer parameter (referred as \f$\tau\f$) relative to each behavior. 
 *  See the documentation of each derived class to know what the time constant means for this class.
 *  
 *  The different template parameters can be used to instantiate a wide range of AverageProcessor.
 *   - T is the input type of the averaging. It is mandatory to specify this parameter (no default value). 
 *     All other parameters have default values relative to this type.
 *   - U is the output type. Its default value is 'T' (input type = output type)
 *   - W is the weighting type. refer to the DefaultAveraging class for detailed information.
 *     Its default value is double. Note : this type must be able to represent real values.
 *     Unless you have a very good reason, it should not be anything else than float or double
 *   - A is the class that embeds the averaging itself, by realizing a function that can be summarized as :
 *     \f{eqnarray*}Y_{0} &=& X_{0} \\ Y_{k+1} &=& \mathcal{F} \left(\alpha, Y_{k}, \beta, X_{k+1} \right)\f}
 *     Its default value is DefaultAveraging<T,U,W>, which implements :
 *     \f{eqnarray*} Y_{0} &=& X_{0} \\ Y_{k+1} &=& \alpha Y_{k} + \beta X_{k+1}\f}
 *
 *  For example you can simply want a double or std::complex<double> averager.
 *  In that case you will instantiate an AverageProcessor<double> or an AverageProcessor<std::complex<double>>.
 *  
 *  For more advanced features, you can instantiate the templated class
 *  to have for example an integrated std::complex<double> to double conversion in your averager
 *  (for example if you want to average the norm of each std::complex<double>).
 *
 *  For that you will set T to std::complex<double>, U to double, W to double, and you will need to provide a
 *  class similar to DefaultAveraging but which realizes :
 *  \f{eqnarray*} Y_{0} &=& \left|X_{0}\right| \\ Y_{k+1} &=& \alpha Y_{k} + \beta \left|X_{k+1}\right|\f}
 *  In such case you will instantiate an AverageProcessor<std::complex<double>, double, double, MyAveragingFunction>
 *
 *  After you have chosen the template parameters, you can instantiate the template class and use it.
 *  The class is initialized through the init() method, where you set the number of samples in each vector to be averaged
 *  and the time constant that will be used to control the averaging process.
 *
 */
template <typename T, typename U = T, typename W = double, class A = DefaultAveraging<T,U,W> >
class AverageProcessor
{
  public:
    /**
     *  \brief Constructor
     *
     *  Initializes the instance with a default time constant of 1 and a default smaples number of 1
     */
    AverageProcessor() {this->_timeConstant = 1; this->_nbSamples = 1;};
    
    /**
     *  \brief Destructor
     */
    virtual ~AverageProcessor() {};
    
    /**
     *  \brief Processes a vector of input samples
     *  \param[in] data A pointer to a buffer of elements of type T. The number of elements pointed to must match the #_nbSamples value
     */
    virtual bool process(const T* data) = 0;

    /**
     *  \brief Restarts the averaging from the beginning (clear all stored state-data, but keeps the time constant and number of samples)
     */
    virtual void restart() {this->_averaging.reset();};

    /**
     *  \brief Returns the averaged vector
     */
    const U* getOutput() const {return(this->_availableData);};

    /**
     *  \brief Returns the number of input vectors still-to-be-passed to achieve the next averaging
     */
    virtual int getMissingInput() const = 0;
   
    /**
     *  \brief Initializes the instance
     */
    void init(size_t nbSamples, unsigned int timeConstant)
    {
      this->_timeConstant = timeConstant;
      this->_nbSamples = nbSamples;
      this->_averaging.init(nbSamples);
    };
    
  protected:
    unsigned int _timeConstant; ///< the integer number that controls the temporal averaging
    size_t _nbSamples; ///< the number of samples in each vector
    const U* _availableData; ///< the output vector
    A _averaging; ///< the averaging class used for the averaging formulas
};





/**
 *  \brief Implements a Moving Average algorithm
 *
 *  The moving average algorithm averages the most recent \f$\tau\f$ mesurements, \f$\tau\f$ being
 *  the 'time constant' parameter.
 *  This means that the class stores the input vectors until the time constant of \f$\tau\f$
 *  has been reached. When the 'history' is full (when there is \f$\tau\f$ input vectors stored),
 *  the average is computed.
 *  Then, each new input vector replaces the older in the averaging process.
 *
 *  Each input vector has the same weight of \f$\frac{1}{\tau}\f$ in the averaging
 *
 */
template <typename T, typename U = T, typename W = double, class A = DefaultAveraging<T,U,W> >
class MovingAverageProcessor : public AverageProcessor <T, U, W, A>
{
  public:

    /**
     *  \brief Default Constructor
     */
    MovingAverageProcessor() {};
    
    /**
     *  \brief Destructor
     */
    virtual ~MovingAverageProcessor()
    {      
      while(!this->_history.empty())
      {
        delete [] this->_history.front();
        this->_history.pop_front();
      }
    };

    void restart()
    {
      AverageProcessor<T, U, W, A>::restart();
      while(!this->_history.empty())
      {
        delete [] this->_history.front();
        this->_history.pop_front();
      }
    };

    bool process(const T* data)
    {
      bool ret = false;
      this->_history.push_back(cloneInputData(data));
      
      if (getMissingInput() == 0)
      {
        for (unsigned int i = 0; i < this->_timeConstant; i++)
        {
          W newWeight = static_cast<W>(1.) / static_cast<W>(i + 1);
          W historyWeight = static_cast<W>(1.) - newWeight;
          this->_availableData = this->_averaging.process(this->_history[i], newWeight, historyWeight);
        }
        delete [] this->_history.front();
        this->_history.pop_front();
        ret = true;
      }
      return(ret);
    };
    
    int getMissingInput() const { return(this->_timeConstant - this->_history.size()); };

  private:

    T* cloneInputData(const T* data)
    {
      T* clone = new T [this->_nbSamples];
      for (size_t i = 0; i < this->_nbSamples; i++)
        clone[i] = data[i];
      
      return(clone);
    };

    std::deque<T*> _history; ///< the history of input vectors
};




/**
 *  \brief Implements a One Shot algorithm
 *
 *  The one shot algorithm averages only the first \f$\tau\f$ mesurements
 *  (\f$\tau\f$ being the time constant).
 *  After \f$\tau\f$ mesurements have been integrated, each successive input
 *  vector does not change the averaging result
 *
 */
template <typename T, typename U = T, typename W = double, class A = DefaultAveraging<T,U,W> >
class OneShotAverageProcessor : public AverageProcessor <T, U, W, A>
{
  public:

    OneShotAverageProcessor() {this->_nbStored = 0;};
    
    virtual ~OneShotAverageProcessor() {};

    void restart()
    {
      AverageProcessor<T, U, W, A>::restart();
      this->_nbStored = 0;
    };

    bool process(const T* data)
    {
      if (getMissingInput() > 0)
      {
        this->_nbStored++;
        W newWeight = static_cast<W>(1.) / static_cast<W>(this->_nbStored);
        W historyWeight = static_cast<W>(1.) - newWeight;
        this->_availableData = this->_averaging.process(data, newWeight, historyWeight);
      }

      return(getMissingInput() == 0);
    };
    
    int getMissingInput() const { return( (this->_nbStored < this->_timeConstant) ? (this->_timeConstant - this->_nbStored) : 0);};

  private:
    size_t _nbStored;
};






/**
 *  \brief Implements an Auto Restart algorithm
 *
 *  The auto restart averaging repeats the one shot algorithm after every \f$\tau\f$ mesurements
 *
 *  The output data is ready only each \f$\tau\f$ multiple of calls to process()
 *
 *  \sa OneShotAverageProcessor
 */
template <typename T, typename U = T, typename W = double, class A = DefaultAveraging<T,U,W> >
class AutoRestartAverageProcessor : public AverageProcessor <T, U, W, A>
{
  public:

    AutoRestartAverageProcessor() {this->_nbStored = 0;};
    
    virtual ~AutoRestartAverageProcessor() {};

    void restart()
    {
      AverageProcessor<T, U, W, A>::restart();
      this->_nbStored = 0;
    };

    bool process(const T* data)
    {
      if (getMissingInput() == 0)
        this->restart();

      this->_nbStored++;
      W newWeight = static_cast<W>(1.) / static_cast<W>(_nbStored);
      W historyWeight = static_cast<W>(1.) - newWeight;
      this->_availableData = this->_averaging.process(data, newWeight, historyWeight);

      return(getMissingInput() == 0);
    };
    
    int getMissingInput() const { return( (this->_nbStored < this->_timeConstant) ? (this->_timeConstant - this->_nbStored) : 0);};

  private:
    size_t _nbStored;
};


/**
 *  \brief Implements an Linear Continuous algorithm
 *
 *  The linear continuous algorithm averages each input vector with equal weight.
 *
 *  When used with the DefaultAveraging template class, it implements :
 *  \f{eqnarray*} Y_{0} &=& X_{0} \\ Y_{k+1} &=& \left( 1 - \frac{1}{k+1} \right) Y_{k} + \frac{1}{k+1} X_{k+1}\f}
 *
 *  The output data is not ready until \f$\tau\f$ input vectors have been passed,
 *  \f$\tau\f$ being the time constant parameter.
 *  After that, each new input updates the average result.
 */
template <typename T, typename U = T, typename W = double, class A = DefaultAveraging<T,U,W> >
class LinearContinuousAverageProcessor : public AverageProcessor <T, U, W, A>
{
  public:

    LinearContinuousAverageProcessor() {this->_nbStored = 0;};
    
    virtual ~LinearContinuousAverageProcessor() {};

    void restart()
    {
      AverageProcessor<T, U, W, A>::restart();
      this->_nbStored = 0;
    };

    bool process(const T* data)
    {
      this->_nbStored++;
      W newWeight = static_cast<W>(1.) / static_cast<W>(this->_nbStored);
      W historyWeight = static_cast<W>(1.) - newWeight;
      this->_availableData = this->_averaging.process(data, newWeight, historyWeight);

      return(getMissingInput() == 0);
    };
    
    int getMissingInput() const { return( (this->_nbStored < this->_timeConstant) ? (this->_timeConstant - this->_nbStored) : 0);};

  private:
    size_t _nbStored;
};





/**
 *  \brief Implements an Exponential Continuous algorithm
 *
 *  The exponential continuous algorithm averages continuously the input vectors.
 *  Compared to the linear continuous algorithm, the exponential continuous algorithm is
 *  used to give more emphasis on the most recent data, even though it integrates all
 *  input vectors from the beginning of the averaging.
 *
 *  When used with the DefaultAveraging template class, it implements :
 *  \f{eqnarray*} Y_{0} &=& X_{0} \\ Y_{k+1} &=& \alpha_k Y_{k} + \beta_k X_{k+1}\f}
 *  with
 *  - \f$\alpha_k = 1 - \frac{1}{k+1}\f$ and \f$\beta_k = \frac{1}{k+1}\f$ if \f$k \leq N\f$
 *  - \f$\alpha_k = 1 - \frac{1}{N+1}\f$ and \f$\beta_k = \frac{1}{N+1}\f$ if \f$k > N\f$
 *
 *  The output data is not ready until \f$\tau\f$ input vectors have been passed. 
 *  After that, each new input updates the average result.
 */
template <typename T, typename U = T, typename W = double, class A = DefaultAveraging<T,U,W> >
class ExponentialContinuousAverageProcessor : public AverageProcessor <T, U, W, A>
{
  public:

    ExponentialContinuousAverageProcessor() {_nbStored = 0;};
    
    virtual ~ExponentialContinuousAverageProcessor() {};

    void restart()
    {
      AverageProcessor<T, U, W, A>::restart();
      this->_nbStored = 0;
    };

    bool process(const T* data)
    {
      this->_nbStored++;

      W newWeight, historyWeight;
      if (this->_nbStored <= this->_timeConstant)
        newWeight = static_cast<W>(1.) / static_cast<W>(this->_nbStored);
      else
        newWeight = static_cast<W>(1.) / static_cast<W>(this->_timeConstant);
      
      historyWeight = static_cast<W>(1.) - newWeight;

      this->_availableData = this->_averaging.process(data, newWeight, historyWeight);

      return(getMissingInput() == 0);
    };
    
    int getMissingInput() const { return( (this->_nbStored < this->_timeConstant) ? (this->_timeConstant - this->_nbStored) : 0);};

  private:
    size_t _nbStored;
};

#endif /* AVERAGEPROCESSOR_H_HEADER_INCLUDED_BC276C6A */
