#ifndef INCLUDE_SPECTRUMPROCESSORBASE_HPP_HEADER_INCLUDED_BC21EDDA
#define INCLUDE_SPECTRUMPROCESSORBASE_HPP_HEADER_INCLUDED_BC21EDDA

#include "NullPointerException.h"


namespace FFT
{

class Spectrum;

/**
 *  \brief Abstract base class for processing a spectrum contained in a Data instance
 */

//##ModelId=43CE4F1C00E1
class SpectrumProcessorBase
{
  public:
    /**
     *  \brief Constructor
     */
    //##ModelId=43E215770365
    SpectrumProcessorBase();

    /**
     *  \brief Destructor
     */
    //##ModelId=43DF94880210
    virtual ~SpectrumProcessorBase();

    /**
     *  \brief Processes a Spectrum instances
     *
     *  \exception NullPointerException if no data is available
     *  \return A boolean indicating if output data is ready
     */
    //##ModelId=43CE4FCF0106
    virtual bool process(const Spectrum& fftData) = 0 ;


    /**
     *  \brief Returns the data available in output
     *
     *  \exception NullPointerException if no data is available
     */
    //##ModelId=43D5F8BA03C9
    virtual const Spectrum& getData() const throw(NullPointerException);



  protected:
    //##ModelId=43D5F8A003E7
    Spectrum* _availableData; ///< output of the processor
};

}

#endif /* INCLUDE_SPECTRUMPROCESSORBASE_HPP_HEADER_INCLUDED_BC21EDDA */
