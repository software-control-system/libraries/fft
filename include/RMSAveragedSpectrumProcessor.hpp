#ifndef INCLUDE_RMSAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC20E887
#define INCLUDE_RMSAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC20E887

#include "AverageProcessor.h"
#include "AveragedSpectrumProcessorBase.hpp"


namespace FFT
{
/**
 *  \brief Base class for all RMS-based averaged spectrum processor
 *
 *  RMS means here that the averaging is done on the Root Mean Square of the input data.
 *
 *  Its specificity is to maintain an AverageProcessor<double>.
 *  The RMS computation are done just before providing the RMS values of the input spectrum
 */

//##ModelId=43DE491B0113
class RMSAveragedSpectrumProcessor : public AveragedSpectrumProcessorBase
{
  public:
    /**
     *  \brief Constructor
     *
     *  \param[in] method the kind of temporal averaging (one shot, moving average...)
     *  \sa TimeAveragingMethod
     */
    //##ModelId=43DF3BA8004B
    RMSAveragedSpectrumProcessor(TimeAveragingMethod method) throw(IndexOutOfBoundException);

    /**
     *  \brief Destructor
     */
    //##ModelId=43DF949F033A
    virtual ~RMSAveragedSpectrumProcessor();

    /**
     *  \brief Return the number of spectrum still to be passed to get the next output ready
     */
    //##ModelId=43DE16DB0225
    size_t getNbMissingSpectrum() const;

    /**
     *  \brief Restart the averaging process but keeps the time constant to its previous value
     */
    //##ModelId=43E200500328
    void restart();


  protected:
    /**
     *  \brief Sends the sample number to the AverageProcessor class
     */
    //##ModelId=43E07C48019F
    void initAverageProcessor();
    
    //##ModelId=43DE492E0362
    AverageProcessor<double>* _processor; ///< The average processor class that does the averaging stuff 
    

};
}


#endif /* INCLUDE_RMSAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC20E887 */
