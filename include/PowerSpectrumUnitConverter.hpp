#ifndef INCLUDE_POWERSPECTRUMUNITCONVERTER_HPP_HEADER_INCLUDED_BC162775
#define INCLUDE_POWERSPECTRUMUNITCONVERTER_HPP_HEADER_INCLUDED_BC162775

#include "SpectrumProcessorBase.hpp"

#include "IllegalArgumentException.h"


namespace FFT
{

class FFTProcessorConfig;
class Spectrum;


/**
 *  \brief The available units for Power Spectrum
 */
typedef enum
{
  VOLT_SQUARE, ///< \f$V^2\f$
  DECIBEL_VOLT_SQUARE, ///< \f$dBV\f$
  DECIBEL_MILLI_VOLT_SQUARE, ///< \f$dBmV\f$
  DECIBEL_MICRO_VOLT_SQUARE, ///< \f$dB \mu V\f$
  WATT, ///< \f$W\f$
  DECIBEL_WATT, ///< \f$dBW\f$
  DECIBEL_MILLI_WATT ///< \f$dBmW\f$
} PowerSpectrumUnits;

/**
 *  \brief The possible peak scaling factor
 */
typedef enum
{
  RMS, ///< with RMS peak scaling, the signal \f$x_k = \alpha \sin(2 \pi \nu k)\f$ has a peak value of \f$\frac{\alpha}{\sqrt{2}}\f$ so its power spectrum has a peak value of \f$\frac{\alpha^2}{2}\f$
  PEAK ///< with PEAK peak scaling, the signal \f$x_k = \alpha \sin(2 \pi \nu k)\f$ has a peak value of \f$\alpha\f$ so its power spectrum has a peak value of \f$\alpha^2\f$
} PeakScalingMethod;


/**
 *  \brief Converts real Power Spectrum from the default unit (\f$V_{RMS}^2\f$) to a user-specified unit
 *
 *  The default unit of Power Spectrum delivered by all AveragedSpectrumProcessorBase derived classes
 *  is the \f$V_{RMS}^2\f$, which means that the power spectrum of a sinusoid of the form \f$x_k = \alpha \sin(2 \pi \nu k)\f$
 *  has a maximum value of \f$\frac{\alpha^2}{2} = \left( \frac{\alpha}{\sqrt{2}}\right)^2\f$
 *
 *  Using PowerSpectrumUnitConverter allows you to change the units in which a power spectrum is represented.
 *  You can choose :
 *  - to represent the power in \f$V_{RMS}^2\f$ or in \f$W\f$ (for that you must provide an impedance)
 *  - to use a linear or logarithmic axis
 *  - the peak scaling factor to represent RMS values or PEAK values
 *  - to transform the power spectrum into a power spectral density spectrum
 *
 * The Power Spectral Density can be computed from the Power Spectrum through:
 * \f[
 *    PSD\left[\frac{V_{RMS}^2}{Hz}\right] = \frac{POWER}{ENBW \times df[Hz]}
 * \f]
 * where ENBW corresponds to the Equivalent Noise Bandwidth of the window (\f$ = \frac{1}{N} \sum_{k=1}^{N} w_k^2 \f$)
 *
 * The logarithmic scaling is done through this formula :
 * \f[
 *    POWER[dBmV] = 10 log_{10} \left( 1000 \left[ \frac{mW}{W} \right] \times POWER \left[V_{RMS}^2\right]\right)
 * \f]
 *
 */
//##ModelId=43E9D1C4013C
class PowerSpectrumUnitConverter : public SpectrumProcessorBase
{
  public:
    /**
     *  \brief Constructor
     */
    //##ModelId=43E9D1F50108
    PowerSpectrumUnitConverter();

    /**
     *  \brief Destructor
     */
    //##ModelId=43E9D1F50118
    virtual ~PowerSpectrumUnitConverter();

    //##ModelId=43E9D1E101B7
    bool process(const Spectrum& fftSpectrumData);

    /**
     *  \brief Passes parameters of unit conversion to the instance
     *  \param[in] units the units in which the power spectrum is converted to
     *  \param[in] impedance the impedance in \f$\Omega\f$
     *  \param[in] peakScalingMethod the peak scaling method of the output
     *  \param[in] psdFlag a boolean indocating if a PSD conversion must take place
     *  \param[in] fftConfig the configuration in which the FFT is computed
     */
    //##ModelId=43E9D23F03C6
    void init(PowerSpectrumUnits units, double impedance, PeakScalingMethod peakScalingMethod, bool psdFlag, const FFTProcessorConfig& fftConfig);

  private:
    //##ModelId=43E9D2A1017C
    bool checkValidity(const Spectrum& data);
    
    //##ModelId=43E9D2B90163
    PowerSpectrumUnits _units; ///< the units into which the input power spectrum is converted

    //##ModelId=43E9D2C302F7
    PeakScalingMethod _peakScalingMethod; ///< the peak scaling method (RMS or PEAK)

    //##ModelId=43E9D2D803DD
    bool _psdFlag; ///< boolean indicating if a PSD conversion must be done

    //##ModelId=43E9D2E202D3
    const FFTProcessorConfig* _fftConfig; ///< the configuration giving information on the spectrum

    //##ModelId=43E9D2F002FA
    double _impedance; ///< the impedance used when converting to Watts

};

}

#endif /* INCLUDE_POWERSPECTRUMUNITCONVERTER_HPP_HEADER_INCLUDED_BC162775 */
