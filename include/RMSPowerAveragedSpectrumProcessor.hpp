#ifndef INCLUDE_RMSPOWERAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1E23CD
#define INCLUDE_RMSPOWERAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1E23CD

#include "RMSAveragedSpectrumProcessor.hpp"

namespace FFT
{

/**
 *  \brief Implementation of the RMS Power Averaging
 *
 *  RMS means here that the averaging is done on the Root Mean Square of the input data.
 *
 *  Let \f$ X_{k} \f$ be the k-th input complex spectrum.
 *  The formula used to compute the averaged spectrum is:
 *  \f[
 *     RMS = Mean_k\left( | X_k |^2 \right) = Mean_k\left(  X_k  X_k^*  \right)
 *  \f]
 */
//##ModelId=43DE46C80194
class RMSPowerAveragedSpectrumProcessor : public RMSAveragedSpectrumProcessor
{
  public:
    /**
     *  \brief Constructor
     *
     *  \param[in] method the kind of temporal averaging (one shot, moving average...)
     *  \sa TimeAveragingMethod
     */
    //##ModelId=43E1DB86034B
    RMSPowerAveragedSpectrumProcessor(TimeAveragingMethod method);

    /**
     *  \brief Destructor
     */
    //##ModelId=43E1DB86034D
    virtual ~RMSPowerAveragedSpectrumProcessor();

    //##ModelId=43E1DB86035B
    bool process(const Spectrum& fftSpectrumData);


};

}

#endif /* INCLUDE_RMSPOWERAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1E23CD */
