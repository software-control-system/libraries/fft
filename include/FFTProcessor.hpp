#ifndef INCLUDE_FFTPROCESSOR_HPP_HEADER_INCLUDED_BC298302
#define INCLUDE_FFTPROCESSOR_HPP_HEADER_INCLUDED_BC298302

#include "InputDataManager.hpp"
#include "Spectrum.hpp"
#include "fftw3.h"
#include "NullPointerException.h"
class Spectrum;

namespace FFT
{

class FFTProcessorConfig;
class Spectrum;

/**
 *  \brief Main class that computes the Fast Fourier Transform
 *
 *  Here is a sample of code using the FFTProcessor class
 *  \code
 *  int main( int argc, char **argv)
 *  {
 *    
 *  }
 *  \endcode
 */

//##ModelId=43C7DF930121
class FFTProcessor
{
  public:
    //##ModelId=43D8F6780039
    FFTProcessor();

    //##ModelId=43D8F67F01E8
    ~FFTProcessor();

    //##ModelId=43CE4F800054
    bool process(const double* inputData, size_t n) throw(NullPointerException);

    //##ModelId=43CE4F800057
    void reset();

    //##ModelId=43CE5D5B010C
    void setConfig(FFTProcessorConfig& config);

    //##ModelId=43CBBADF020E
    const Spectrum& getData() const;

    //##ModelId=43CF72280127
    size_t getNbMissingSamples() const;

    //##ModelId=43DE0EEA02E3
    void restart();

  private:
    //##ModelId=43C7E039038A
    FFTProcessorConfig* _config; ///< Configuration associated with the processor

    //##ModelId=43C7E03E0393
    InputDataManager _inputDataManager; ///< Stores input data and provide them to the processor

    //##ModelId=43CB5FD60284
    bool _spectrumReady; ///< Indicates if output data is ready; returned at each process() call

    //##ModelId=43CBB9990370
    Spectrum* _availableFFTData; ///< Output data of the processor
    
    //##ModelId=43DDDFEE00D1
    double* _inputData; ///< Buffer used to retrieve data from #_inputDataManager and multiply them with the window values

    //##ModelId=43D9029801ED
    fftw_plan _plan; ///< Internal FFTW data structure used to compute the FFT
};

}

#endif /* INCLUDE_FFTPROCESSOR_HPP_HEADER_INCLUDED_BC298302 */
