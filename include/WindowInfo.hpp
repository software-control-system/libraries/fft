#ifndef INCLUDE_WINDOWINFO_HPP_HEADER_INCLUDED_BC04D199
#define INCLUDE_WINDOWINFO_HPP_HEADER_INCLUDED_BC04D199

#include <vector>

namespace FFT
{

/**
 *  \brief Enumeration for the different types of supported windows
 *
 * In the following formulas, \f$ w_k \f$ represents the window sample value, for \f$ k \in [0, N - 1] \f$. For all other \f$ k \f$, \f$ w_k = 0 \f$
 *
 * Formulas & Coefficients were taken from : 
 * http://zone.ni.com/reference/en-XX/help/lv/71/lvwave/Scaled_Window/
 */

typedef enum
{
  RECTANGULAR, ///< The rectangular window (= no window)
               ///< \f[ w_k = 1 \f]

  BLACKMAN, ///< The Blackman window
            ///< \f[ w_k = 0.42 - 0.5 \cos(2 \pi \frac{k}{N}) + 0.08 \cos(4 \pi \frac{k}{N})\f]

  EXACT_BLACKMAN, ///< The exact Blackman window
                  ///< \f{eqnarray*} w_k &=& 0.42659071367153911200 - 0.49656061908856408100 \cos(2 \pi \frac{k}{N}) \\ & & + 0.07684866723989682010 \cos(4 \pi \frac{k}{N}) \f}

  HAMMING, ///< The Hamming window
           ///< \f[ w_k = 0.54 - 0.46 \cos(2 \pi \frac{k}{N})\f]

  HANN, ///< The Hann (also called Hanning) window
        ///< \f[ w_k = 0.5 - 0.5 \cos(2 \pi \frac{k}{N})\f]

  FLATTOP, ///< The Flat Top window
           ///< \f{eqnarray*} w_k &=& 0.215578948 - 0.41663158 \cos(2 \pi \frac{k}{N}) + 0.277263158 \cos(4 \pi \frac{k}{N}) \\ & & - 0.083578947 \cos(6 \pi \frac{k}{N}) + 0.006947368 \cos(8 \pi \frac{k}{N})\f}

  BLACKMAN_HARRIS_3,///< The 3 - term Blackman-Harris window
                    ///< \f[ w_k = 0.42323 - 0.49755 \cos(2 \pi \frac{k}{N}) + 0.07922 \cos(4 \pi \frac{k}{N})\f]

  BLACKMAN_HARRIS_4,///< The 4 - term Blackman-Harris window
                    ///< \f[ w_k = 0.35875 - 0.48829 \cos(2 \pi \frac{k}{N}) + 0.14128 \cos(4 \pi \frac{k}{N}) - 0.01168 \cos(6 \pi \frac{k}{N})\f]

  BLACKMAN_HARRIS_7,///< The 7 - term Blackman-Harris window
                    ///< \f{eqnarray*} w_k &=& 0.27105140069342415 - 0.43329793923448606 \cos(2 \pi \frac{k}{N}) \\ & & + 0.21812299954311062 \cos(4 \pi \frac{k}{N}) - 0.065925446388030898 \cos(6 \pi \frac{k}{N}) \\ & & + 0.010811742098372268 \cos(8 \pi \frac{k}{N}) - 7.7658482522509342 . 10^{-4} \cos(10 \pi \frac{k}{N}) \\ & &+ 1.3887217350903198 . 10^{-5} \cos(12 \pi \frac{k}{N})\f}

  LOW_SIDELOBE ///< The Low SideLobe window
               ///< \f{eqnarray*} w_k &=& 0.323215218 - 0.471492057 \cos(2 \pi \frac{k}{N}) + 0.17553428 \cos(4 \pi \frac{k}{N}) \\ & & - 0.028497078 \cos(6 \pi \frac{k}{N}) + 0.001261367 \cos(8 \pi \frac{k}{N})\f}

} WindowType;


/**
 *  \brief Contains all the information relative to the window used in FFT computation, but not the actual values
 */
//##ModelId=43FB44D800CC
class WindowInfo
{
  public:
    /**
     *  \brief Constructor
     *
     *  \param[in] type the type of window
     *  \param[in] size the number of samples in the time domain
     */
    //##ModelId=43FB44EC0361
    WindowInfo(WindowType type, size_t size);

    /**
     *  \brief Destructor
     */
    //##ModelId=43FB44EC0371
    ~WindowInfo();

    /**
     *  \brief Returns the type of window
     */
    //##ModelId=43FB44EC0372
    WindowType getWindowType() const;

    /**
     *  \brief Returns the number of samples
     */
    //##ModelId=43FB44EC0380
    size_t getSize() const;

    /**
     *  \brief Returns the Equivalent Noise Bandwidth, which is \f$ \frac{1}{N} \sum_{k = 1}^{N} w_k^2 \f$
     */
    //##ModelId=43FB44EC0390
    double getENBW() const;

    /**
     *  \brief Returns the Coherent Gain, which is \f$ \frac{1}{N} \sum_{k = 1}^{N} w_k \f$
     */
    //##ModelId=43FB44EC0391
    double getCoherentGain() const;

    /**
     *  \brief Returns the 6 dB bandwidth, which is the width in bin units, for the frequency response to decrease from 6 dB
     */
    //##ModelId=43FB44EC0392
    double get6dB_BW() const;
    
    /**
     *  \brief Returns the number of coefficients for the window
     */
    //##ModelId=43FC3500026A
    size_t getNbCoeffs() const;

    /**
     *  \brief Returns a specific coefficient
     */
    //##ModelId=43FC354101A4
    double getCoeff(size_t i) const;
    
    /**
     *  \brief Copy constructor
     */
    //##ModelId=43FC630100E2
    WindowInfo(const WindowInfo& right);


  private:
    //##ModelId=43FB44F1023D
    WindowType _type; ///< the kind of window

    //##ModelId=43FB44F1024C
    size_t _size; ///< the number of samples

    //##ModelId=43FB44F1025C
    double _enbw; ///< the equivalent noise bandwidth

    //##ModelId=43FB44F1025D
    double _coherentGain; ///< the coherent gain

    //##ModelId=43FB44F1026C
    double _6dB_BW; ///< the 6 dB bandwidth
    
    //##ModelId=43FC2EB10145
    std::vector<double> _coeffs; ///< the coefficient of the window used to generate the samples



};

}

#endif /* INCLUDE_WINDOWINFO_HPP_HEADER_INCLUDED_BC04D199 */
