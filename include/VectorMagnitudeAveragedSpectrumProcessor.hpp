#ifndef INCLUDE_VECTORMAGNITUDEAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1D859E
#define INCLUDE_VECTORMAGNITUDEAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1D859E
#include "VectorAveragedSpectrumProcessor.hpp"
class Spectrum;

namespace FFT
{

class Data;

/**
 *  \brief Implementation of the Vector Magnitude Averaging
 *
 *  Vector Magnitude Averaging means here that the averaging is done on complex data.
 *  The output is a COMPLEX spectrum
 *
 *  Let \f$ X_{k} \f$ be the k-th input complex spectrum.
 *  The formula used to compute the averaged spectrum is:
 *  \f[
 *     VectorAv = Mean_k\left( X_k \right)
 *  \f]
 */
//##ModelId=43DE46AE02FD
class VectorMagnitudeAveragedSpectrumProcessor : public VectorAveragedSpectrumProcessor
{
  public:
    /**
     *  \brief Constructor
     *
     *  \param[in] method the kind of temporal averaging (one shot, moving average...)
     *  \sa TimeAveragingMethod
     */
    //##ModelId=43E1DBB302D6
    VectorMagnitudeAveragedSpectrumProcessor(TimeAveragingMethod method);

    /**
     *  \brief Destructor
     */
    //##ModelId=43E1DBB302E6
    virtual ~VectorMagnitudeAveragedSpectrumProcessor();

    //##ModelId=43E1DBB302E7
    bool process(const Spectrum& fftSpectrumData);

};
}


#endif /* INCLUDE_VECTORMAGNITUDEAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC1D859E */
