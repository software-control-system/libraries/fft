#ifndef INCLUDE_TOOLS_HPP_HEADER_INCLUDED_BC1617E0
#define INCLUDE_TOOLS_HPP_HEADER_INCLUDED_BC1617E0

#if defined(_WIN32) || defined(WIN32)
#if defined(_MSC_VER)
// disable warning C4275: non dll-interface class 'std::_Complex_base<float>' used as base for dll-interface class 'std::complex<float>' 
#pragma warning ( disable : 4275 )
#endif
#endif

#include <complex>

#if defined(_WIN32) || defined(WIN32)
#if defined(_MSC_VER)
#pragma warning ( default : 4275 )
#endif
#endif


#include "NullPointerException.h"


namespace FFT
{
/**
 *  \brief Gathers all common unitary processing functions used throughout the implementation
 */
//##ModelId=43E9BA900026
class Tools
{
  public:
    //##ModelId=43DF7C700207
    static double* computeSquareNorm(const std::complex<double>* data, size_t n) throw(NullPointerException);

    //##ModelId=43DF8211012B
    static double* computeNorm(const std::complex<double>* data, size_t n) throw(NullPointerException);

};
}


#endif /* INCLUDE_TOOLS_HPP_HEADER_INCLUDED_BC1617E0 */
