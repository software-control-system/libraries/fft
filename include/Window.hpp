#ifndef INCLUDE_WINDOW_HPP_HEADER_INCLUDED_BC29B1EF
#define INCLUDE_WINDOW_HPP_HEADER_INCLUDED_BC29B1EF

#include <stddef.h> // for size_t
#include "WindowInfo.hpp"


namespace FFT
{
/**
 *  \brief Represents a windowing function used in FFT computation
 */

//##ModelId=43D4F11E006E
class Window
{
  public:
    /**
     *  \brief Constructor
     *
     *  \param[in] type the type of window
     *  \param[in] N the number of samples in the time domain
     */
    //##ModelId=43D4F2500082
    Window(WindowType type, size_t N);

    /**
     *  \brief Destructor
     */
    //##ModelId=43D8A46303B5
    ~Window();

    /**
     *  \brief Returns the samples of the window, already scaled by the coherent gain
     */
    //##ModelId=43D4F26A00A4
    const double* getValues() const;
    
    /**
     *  \brief Returns the window settings
     */
    //##ModelId=43FC38FA0087
    const WindowInfo& getWindowInfo() const;
    
    /**
     *  \brief Returns the type of window
     */
    //##ModelId=43FC39AB0167
    WindowType getWindowType() const;




  private:

    //##ModelId=43D4F13601B9
    double* _values; ///< the sample values
    
    //##ModelId=43FC2A520217
    WindowInfo* _windowInfo; ///< the window settings



    
};

}

#endif /* INCLUDE_WINDOW_HPP_HEADER_INCLUDED_BC29B1EF */
