#ifndef INCLUDE_AVERAGEDSPECTRUMPROCESSORBASE_HPP_HEADER_INCLUDED_BC218BEB
#define INCLUDE_AVERAGEDSPECTRUMPROCESSORBASE_HPP_HEADER_INCLUDED_BC218BEB

#include "SpectrumProcessorBase.hpp"
#include "Spectrum.hpp" // for ScalarType

#include "ValueOutOfBoundException.h"
#include "IndexOutOfBoundException.h"
#include "IllegalArgumentException.h"


namespace FFT
{


/**
 *  \brief Enumeration for the different flavors for time averaging
 *
 *  All time averaging method are relative to a time constant, 
 *  which is an unsigned integer parameter of the processor.
 *  This time constant will be denoted as \f$ \tau \f$ in the sequel.
 *
 * \sa AveragedSpectrumProcessorBase::init()
 */
typedef enum
{
  /**
   *  The averaging process stops after \f$ \tau \f$ has been reached.
   *  The average result does not change with subsequent call to process().
   *  Each FFT frame has the same weight.
   */
  ONE_SHOT,
  /**
   *  The averaging automatically restarts after \f$ \tau \f$ has been reached.
   *  At each call to process(), a check is done to see if data was ready last time.
   *  In that case, the average processor is automatically restarted and averaging continues.
   *  Each FFT frame has the same weight.
   */
  AUTO_RESTART,
  /**
   *  The averaging is done linearly over the last \f$ \tau \f$ FFT frames.
   *  It begins in the same way as the one shot mode but after \f$ \tau \f$ has been reached,
   *  each subsequent FFT frame replaces the oldest in the average result.
   *  Each FFT frame has the same weight.
   */
  MOVING_AVERAGE,
  /**
   *  The averaging process continues indefinitely, as if \f$ \tau \f$ increments
   *  with each new frame.
   *  Each FFT frame has equal weighting in the average.
   *  Each FFT frame has the same weight.
   */
  LINEAR_CONTINUOUS,
  /**
   *  The averaging process continues indefinitely, as if \f$ \tau \f$ increments
   *  with each new frame.
   *  It behaves as the one shot algorithm for the first \f$ \tau \f$ frames.
   *  After \f$ \tau \f$ has been reached, each subsequent FFT frame has a 
   *  fixed weight \f$ \frac{1}{\tau} \f$, such that recent FFT frames have 
   *  a greater impact than in #LINEAR_CONTINUOUS.
   */
  EXPONENTIAL_CONTINUOUS
} TimeAveragingMethod;

/**
 *  \brief Abstract base class for spectrum averaging
 */

//##ModelId=43C6286B032F
class AveragedSpectrumProcessorBase : public SpectrumProcessorBase
{
  public:
    /**
     *  \brief Constructor
     */
    //##ModelId=43D5F9460051
    AveragedSpectrumProcessorBase();

    /**
     *  \brief Destructor
     */
    //##ModelId=43DF949A0133
    virtual ~AveragedSpectrumProcessorBase();

    /**
     *  \brief Initializes the instance with a specified time constant
     * 
     *  \param[in] timeConstant the time constant
     */
    //##ModelId=43CE52910394
    void init(unsigned int timeConstant);

    /**
     *  \brief Restart the averaging process but keeps the time constant to its previous value
     */
    //##ModelId=43DE112D0214
    virtual void restart() = 0;

    /**
     *  \brief Return the number of spectrum still to be passed to get the next output ready
     */
    //##ModelId=43DE12E202D2
    virtual size_t getNbMissingSpectrum() const = 0;

    
    
  protected:
    /**
     *  \brief Sends the sample number to the AverageProcessor class
     */
    //##ModelId=43E0803B0085
    virtual void initAverageProcessor() = 0;

    /**
     *  \brief Initialize and allocates data suitable for processing the spectrum passed in parameter
     * 
     *  \param[in] fftSpectrumData a spectrum passed to the processor
     */
    //##ModelId=43E1FF23011D
    void firstTimeInit(const Spectrum& fftSpectrumData);


    //##ModelId=43DF2D9701EB
    unsigned int _timeConstant; ///< the time constant of the averaged processor
    //##ModelId=43E1E9A60312
    bool _firstTime; ///< a boolean indicating if process() has already been called once
    //##ModelId=43E22058017C
    ScalarType _outputType; ///< the data type in output of the processor
    //##ModelId=43E9BC1C030B
    size_t _nbBins; ///< the number of samples contained in the spectrum passed to this instance
    
};

}

#endif /* INCLUDE_AVERAGEDSPECTRUMPROCESSORBASE_HPP_HEADER_INCLUDED_BC218BEB */
