#ifndef INCLUDE_RMSMAGNITUDEAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC20954A
#define INCLUDE_RMSMAGNITUDEAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC20954A

#include "RMSAveragedSpectrumProcessor.hpp"

namespace FFT
{

/**
 *  \brief Implementation of the Peak Hold Power Averaging
 *
 *  Peak Hold Power Averaging retains the RMS peak levels of the averaged quantities.
 *  It performs peak-hold at each frequency bin separately to retain peak RMS levels
 *  from one FFT frame to the next.
 *
 *  Let \f$ X_{k} \f$ be the k-th input complex spectrum.
 *  The formula used to compute the averaged spectrum is:
 *  \f[
 *     PeakHoldAverage = Max_k\left( | X_k | \right) = Max_k\left( \sqrt{X_k  X_k^*} \right)
 *  \f]
 */
//##ModelId=43DE467C0242
class RMSMagnitudeAveragedSpectrumProcessor : public RMSAveragedSpectrumProcessor
{
  public:
    /**
     *  \brief Constructor
     *
     *  \param[in] method the kind of temporal averaging (one shot, moving average...)
     *  \sa TimeAveragingMethod
     */
    //##ModelId=43DF3B7701A0
    RMSMagnitudeAveragedSpectrumProcessor(TimeAveragingMethod method);

    /**
     *  \brief Destructor
     */
    //##ModelId=43DF94A3037C
    virtual ~RMSMagnitudeAveragedSpectrumProcessor();

    //##ModelId=43CD10B90169
    bool process(const Spectrum& fftSpectrumData);

};

}

#endif /* INCLUDE_RMSMAGNITUDEAVERAGEDSPECTRUMPROCESSOR_HPP_HEADER_INCLUDED_BC20954A */
