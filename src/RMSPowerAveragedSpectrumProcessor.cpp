#include "RMSPowerAveragedSpectrumProcessor.hpp"
#include "Tools.hpp"
#include "Spectrum.hpp"

namespace FFT {

//##ModelId=43E1DB86034B
RMSPowerAveragedSpectrumProcessor::RMSPowerAveragedSpectrumProcessor(TimeAveragingMethod method)
: RMSAveragedSpectrumProcessor(method)
{
}


//##ModelId=43E1DB86034D
RMSPowerAveragedSpectrumProcessor::~RMSPowerAveragedSpectrumProcessor()
{
}

//##ModelId=43E1DB86035B
bool RMSPowerAveragedSpectrumProcessor::process(const Spectrum& fftSpectrumData)
{
  this->firstTimeInit(fftSpectrumData);
  double* rmsValue = Tools::computeSquareNorm(fftSpectrumData.getComplexData(), _nbBins);
  
  // scale
  unsigned int i;
  for (i = 0; i < _nbBins; i++)
    rmsValue[i] /= fftSpectrumData.getWindowSize();

  bool ready = _processor->process(rmsValue);
  delete [] rmsValue;
  if (ready)
  {
    memcpy(_availableData->getRealData(), _processor->getOutput(), _nbBins * sizeof(double));
  }
  return(ready);
}

}



