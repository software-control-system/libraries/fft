#include "VectorPowerAveragedSpectrumProcessor.hpp"
#include "Tools.hpp"
#include "Spectrum.hpp"

namespace FFT {

//##ModelId=43E1DBD8035B
VectorPowerAveragedSpectrumProcessor::VectorPowerAveragedSpectrumProcessor(TimeAveragingMethod method)
: VectorAveragedSpectrumProcessor(method)
{
}


//##ModelId=43E1DBD8036A
VectorPowerAveragedSpectrumProcessor::~VectorPowerAveragedSpectrumProcessor()
{
}

//##ModelId=43E1DBD8036B
bool VectorPowerAveragedSpectrumProcessor::process(const Spectrum& fftSpectrumData)
{
  this->firstTimeInit(fftSpectrumData);
  bool ready = _processor->process(fftSpectrumData.getComplexData());

  if (ready)
  {
    double* power = Tools::computeSquareNorm(_processor->getOutput(), _nbBins);
    
    // scale
    unsigned int i;
    for (i = 0; i < _nbBins; i++)
      power[i] /= fftSpectrumData.getWindowSize();

    memcpy(_availableData->getRealData(), power, _nbBins * sizeof(double));
    delete [] power;
  }

  return(ready);
}

}

