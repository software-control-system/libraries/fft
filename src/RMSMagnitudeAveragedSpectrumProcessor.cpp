#include "RMSMagnitudeAveragedSpectrumProcessor.hpp"
#include "Tools.hpp"
#include "Spectrum.hpp"

namespace FFT {

//##ModelId=43DF3B7701A0
RMSMagnitudeAveragedSpectrumProcessor::RMSMagnitudeAveragedSpectrumProcessor(TimeAveragingMethod method)
: RMSAveragedSpectrumProcessor(method)
{
}

//##ModelId=43DF94A3037C
RMSMagnitudeAveragedSpectrumProcessor::~RMSMagnitudeAveragedSpectrumProcessor()
{
}

//##ModelId=43CD10B90169
bool RMSMagnitudeAveragedSpectrumProcessor::process(const Spectrum& fftSpectrumData)
{
  this->firstTimeInit(fftSpectrumData);
  double* rmsValue = Tools::computeNorm(fftSpectrumData.getComplexData(), _nbBins);

  bool ready = _processor->process(rmsValue);
  delete [] rmsValue;
  if (ready)
  {
    memcpy(_availableData->getRealData(), _processor->getOutput(), _nbBins * sizeof(double));
  }
  return(ready);
}

}

