#include "RMSAveragedSpectrumProcessor.hpp"

namespace FFT {

//##ModelId=43DF3BA8004B
RMSAveragedSpectrumProcessor::RMSAveragedSpectrumProcessor(TimeAveragingMethod method) throw(IndexOutOfBoundException)
{
  switch(method)
  {
    case ONE_SHOT:
      _processor = new OneShotAverageProcessor<double>;
      break;
    case AUTO_RESTART:
      _processor = new AutoRestartAverageProcessor<double>;
      break;
    case MOVING_AVERAGE:
      _processor = new MovingAverageProcessor<double>;
      break;
    case LINEAR_CONTINUOUS:
      _processor = new LinearContinuousAverageProcessor<double>;
      break;
    case EXPONENTIAL_CONTINUOUS:
      _processor = new ExponentialContinuousAverageProcessor<double>;
      break;
    default:
      throw IndexOutOfBoundException("method",
                                     method,
                                     0,
                                     4,
                                     "RMSAveragedSpectrumProcessor(TimeAveragingMethod method)",
                                     __FILE__, 
                                     __LINE__);
  }
}

//##ModelId=43DF949F033A
RMSAveragedSpectrumProcessor::~RMSAveragedSpectrumProcessor()
{
  if (_processor)
    delete _processor;
}

//##ModelId=43DE16DB0225
size_t RMSAveragedSpectrumProcessor::getNbMissingSpectrum() const
{
  return(_processor->getMissingInput());
}

//##ModelId=43E200500328
void RMSAveragedSpectrumProcessor::restart()
{
  _processor->restart();
  _firstTime = true;
}

//##ModelId=43E07C48019F
void RMSAveragedSpectrumProcessor::initAverageProcessor()
{
  _processor->init(_nbBins, _timeConstant);
}

}






