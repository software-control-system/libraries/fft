#include "PowerSpectrumUnitConverter.hpp"
#include "FFTProcessorConfig.hpp"
#include <float.h> // for DBL_MIN
#include "Spectrum.hpp"

namespace FFT {

//##ModelId=43E9D1F50108
PowerSpectrumUnitConverter::PowerSpectrumUnitConverter()
: _units(DECIBEL_VOLT_SQUARE),
  _peakScalingMethod(RMS),
  _psdFlag(false),
  _fftConfig(0L),
  _impedance(0)
{
}

//##ModelId=43E9D1F50118
PowerSpectrumUnitConverter::~PowerSpectrumUnitConverter()
{
  _fftConfig = 0L;
}

//##ModelId=43E9D1E101B7
bool PowerSpectrumUnitConverter::process(const Spectrum& fftSpectrumData)
{
//  TODO :
//  if (!this->checkValidity(fftSpectrumData))
//    throw IllegalArgumentException();

  if (_availableData)
    delete _availableData;

  size_t fftSize = fftSpectrumData.getNbBins();

  _availableData = new Spectrum(REAL,
                                fftSize, 
                                fftSpectrumData.getWindowInfo(),
                                fftSpectrumData.getSamplingFrequency(),
                                fftSpectrumData.getResolution(),
                                fftSpectrumData.getFFTSize());

  const double* pwSpecData = fftSpectrumData.getRealData();
  double* unitConvPwSpec = _availableData->getRealData();

  double psdScaleFactor = 1 / (_fftConfig->getWindow().getWindowInfo().getENBW() * _fftConfig->getResolution());

  double globalScaleFactor = 1.f;

  // Peak Scaling
  if (_peakScalingMethod == PEAK)
    globalScaleFactor *= 2.f;

  // PSD or not
  if (_psdFlag)
    globalScaleFactor *= psdScaleFactor;

  // TODO
  /*
  switch (_units)
  {
    case WATT:
    case DECIBEL_WATT:
    case DECIBEL_MILLI_WATT:
      if (_impedance == 0)
        throw DivideByZeroException(
      break;
  }
  */

  unsigned int i;
  for (i = 0; i < fftSize; i++)
  {
    double result = pwSpecData[i] * globalScaleFactor;

    // Conversion to dB, Watt,...
    switch (_units)
    {
      case VOLT_SQUARE:
        // nothing to do here
        break;

      case DECIBEL_VOLT_SQUARE:
        result = result == 0.f ? DBL_MIN : 10 * log10(result);
        break;

      case DECIBEL_MILLI_VOLT_SQUARE:
        result = result == 0.f ? DBL_MIN : 10 * log10(1000.f * result);
        break;

      case DECIBEL_MICRO_VOLT_SQUARE:
        result = result == 0.f ? DBL_MIN : 10 * log10(1000000.f * result);
        break;

      case WATT:
        result = result == 0.f ? DBL_MIN : 10 * log10(result / _impedance);
        break;

      case DECIBEL_WATT:
        result = result == 0.f ? DBL_MIN : 10 * log10(1000.f * result / _impedance);
        break;

      case DECIBEL_MILLI_WATT:
        result = result == 0.f ? DBL_MIN : 10 * log10(1000000.f * result / _impedance);
        break;
      
      default:
        // TODO : throw exception
        break;
    }
    unitConvPwSpec[i] = result;
  }

  return(true);
}

//##ModelId=43E9D23F03C6
void PowerSpectrumUnitConverter::init(PowerSpectrumUnits units, double impedance, PeakScalingMethod peakScalingMethod, bool psdFlag, const FFTProcessorConfig& fftConfig)
{
  _units = units;
  _impedance = impedance;
  _peakScalingMethod = peakScalingMethod;
  _psdFlag = psdFlag;
  _fftConfig = &fftConfig;
}

//##ModelId=43E9D2A1017C
bool PowerSpectrumUnitConverter::checkValidity(const Spectrum& data)
{
  return(data.getType() == REAL);
}

}

