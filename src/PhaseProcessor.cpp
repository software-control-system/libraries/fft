#include "PhaseProcessor.hpp"
#include "Spectrum.hpp"
#include "Constant.h"

namespace FFT {

//##ModelId=43E9F3C103AB
PhaseProcessor::PhaseProcessor()
: _units(RADIANS),
  _unWrap(true)
{
}


//##ModelId=43E9F3C103BA
PhaseProcessor::~PhaseProcessor()
{
}

//##ModelId=43E9F34500BE
bool PhaseProcessor::process(const Spectrum& fftSpectrumData)
{
  if (_availableData)
    delete _availableData;

  size_t nbSamples = fftSpectrumData.getNbBins();
  const std::complex<double>* fftRawData = fftSpectrumData.getComplexData();

  _availableData = new Spectrum(REAL,
                                nbSamples, 
                                fftSpectrumData.getWindowInfo(),
                                fftSpectrumData.getSamplingFrequency(),
                                fftSpectrumData.getResolution(),
                                fftSpectrumData.getFFTSize());

  double* phase = _availableData->getRealData();
  
  unsigned int i;
  for (i = 0; i < nbSamples; i++)
  {
    phase[i] = arg(fftRawData[i]);
  }

  if (_unWrap)
    this->unWrap(phase, nbSamples);

  if (_units == DEGREES)
  {
    double scaleFactor = 180 / MATH_CONSTANT::PI;
    for (i = 0; i < nbSamples; i++)
      phase[i] *= scaleFactor;
  }

  return(true);
}

//##ModelId=43E9F35203AF
void PhaseProcessor::init(PhaseUnits units, bool unWrap)
{
  _units = units;
  _unWrap = unWrap;
}

//##ModelId=43E9F5BE03CC
void PhaseProcessor::unWrap(double* phaseValue, size_t n)
{
  double prev = phaseValue[0]; // init to first element so as first value is not corrected
  double curr;
  double jump;
  double unwrapAdjust = 0;
  const double tolerance = MATH_CONSTANT::PI;

  unsigned int i;
  for (i = 0; i < n; i++)
  {
    curr = phaseValue[i]; // goto next value
    curr += unwrapAdjust; // adds the correction used for the previous sample

    jump = curr - prev;
    if( abs(jump) >= tolerance )
    {
      if (jump > 0)
      {
        unwrapAdjust -= 2 * MATH_CONSTANT::PI;
        curr -= 2 * MATH_CONSTANT::PI;
      }
      else
      {
        unwrapAdjust += 2 * MATH_CONSTANT::PI;
        curr += 2 * MATH_CONSTANT::PI;
      }
    }
    prev = curr;
    phaseValue[i] = curr;
  }
}

}

