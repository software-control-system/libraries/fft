#include "VectorAveragedSpectrumProcessor.hpp"

namespace FFT {

//##ModelId=43E202A701D0
VectorAveragedSpectrumProcessor::VectorAveragedSpectrumProcessor(TimeAveragingMethod method) throw(IndexOutOfBoundException)
{
  switch(method)
  {
    case ONE_SHOT:
      _processor = new OneShotAverageProcessor<complex_double>;
      break;
    case AUTO_RESTART:
      _processor = new AutoRestartAverageProcessor<complex_double>;
      break;
    case MOVING_AVERAGE:
      _processor = new MovingAverageProcessor<complex_double>;
      break;
    case LINEAR_CONTINUOUS:
      _processor = new LinearContinuousAverageProcessor<complex_double>;
      break;
    case EXPONENTIAL_CONTINUOUS:
      _processor = new ExponentialContinuousAverageProcessor<complex_double>;
      break;
    default:
      throw IndexOutOfBoundException("method",
                                     method,
                                     0,
                                     4,
                                     "VectorAveragedSpectrumProcessor(TimeAveragingMethod method)",
                                     __FILE__, 
                                     __LINE__);
  }
}

//##ModelId=43E202AB01FF
VectorAveragedSpectrumProcessor::~VectorAveragedSpectrumProcessor()
{
  if (_processor)
    delete _processor;
}

//##ModelId=43E202D103A6
size_t VectorAveragedSpectrumProcessor::getNbMissingSpectrum() const
{
  return(_processor->getMissingInput());
}

//##ModelId=43E202D103B6
void VectorAveragedSpectrumProcessor::restart()
{
  _processor->restart();
  _firstTime = true;
}

//##ModelId=43E202D103B7
void VectorAveragedSpectrumProcessor::initAverageProcessor()
{
  _processor->init(_nbBins, _timeConstant);
}

}





