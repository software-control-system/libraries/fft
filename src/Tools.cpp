#include "Tools.hpp"

namespace FFT {

/**
 *  \brief Compute the square norm of the complex samples given in input
 *
 *  Allocate a buffer of double and compute the square norm of the input in it .
 *  Let \f$ (x_i, y_i) \f$ be the real and imaginary part of the i-th input samples,
 *  \f[ output_i =  x_i^2 + y_i^2\f]
 *
 *
 *  \param[in] data the complex input samples
 *  \param[in] n the size of the buffer
 *  \exception NullPointerException if data is NULL
 */

//##ModelId=43DF7C700207
double* Tools::computeSquareNorm(const std::complex<double>* data, size_t n) throw(NullPointerException)
{
  if (!data)
    throw NullPointerException("data",
                               "Tools::computeSquareNorm(const std::complex<double>* data, size_t n)",
                               __FILE__,
                               __LINE__);

  double* output = new double [n];

  for (unsigned int i = 0; i < n; i++)
    output[i] = std::norm(data[i]);

  return(output);
}

/**
 *  \brief Compute the square norm of the complex samples given in input
 *
 *  Allocate a buffer of double and compute the square norm of the input in it .
 *  Let \f$ (x_i, y_i) \f$ be the real and imaginary part of the i-th input samples,
 *  \f[ output_i =  \sqrt{x_i^2 + y_i^2}\f]
 *
 *
 *  \param[in] data the complex input samples
 *  \param[in] n the size of the buffer
 *  \exception NullPointerException if data is NULL
 */

//##ModelId=43DF8211012B
double* Tools::computeNorm(const std::complex<double>* data, size_t n) throw(NullPointerException)
{
  if (!data)
    throw NullPointerException("data",
                               "Tools::computeNorm(const std::complex<double>* data, size_t n)",
                               __FILE__,
                               __LINE__);

  double* output = Tools::computeSquareNorm(data, n);
  for (unsigned int i = 0; i < n; i++)
    output[i] = sqrt(output[i]);
  return(output);
}

}

