#include "SpectrumProcessorBase.hpp"
#include "Spectrum.hpp"
#include <yat/utils/Logging.h>

namespace FFT {

//##ModelId=43E215770365
SpectrumProcessorBase::SpectrumProcessorBase()
: _availableData(0L)
{
}


//##ModelId=43DF94880210
SpectrumProcessorBase::~SpectrumProcessorBase()
{
YAT_INFO << "\t\t\tSpectrumProcessorBase::~SpectrumProcessorBase()..." << std::endl;
  if (_availableData)
    delete _availableData;
YAT_INFO << "\t\t\tSpectrumProcessorBase::~SpectrumProcessorBase()..." << std::endl;
}


//##ModelId=43D5F8BA03C9
const Spectrum& SpectrumProcessorBase::getData() const throw(NullPointerException)
{
  if (!_availableData){
YAT_INFO << "\t\t\tFATAL SpectrumProcessorBase::getData-> availableData is NULL." << std::endl;
  
    throw NullPointerException("_availableData",
                               "SpectrumProcessorBase::getData()",
                               __FILE__,
                               __LINE__);
 }

  return((const Spectrum&)*_availableData);
}

}

