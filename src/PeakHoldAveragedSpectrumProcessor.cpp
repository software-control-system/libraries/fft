#include "PeakHoldAveragedSpectrumProcessor.hpp"

namespace FFT {


//##ModelId=43E232D0026F
PeakHoldAveragedSpectrumProcessor::PeakHoldAveragedSpectrumProcessor(TimeAveragingMethod method) throw(IndexOutOfBoundException)
{
  switch(method)
  {
    case ONE_SHOT:
      _processor = new OneShotAverageProcessor<double,double,double,PeakHoldAveraging>;
      break;
    case AUTO_RESTART:
      _processor = new AutoRestartAverageProcessor<double,double,double,PeakHoldAveraging>;
      break;
    case MOVING_AVERAGE:
      _processor = new MovingAverageProcessor<double,double,double,PeakHoldAveraging>;
      break;
    case LINEAR_CONTINUOUS:
      _processor = new LinearContinuousAverageProcessor<double,double,double,PeakHoldAveraging>;
      break;
    case EXPONENTIAL_CONTINUOUS:
      _processor = new ExponentialContinuousAverageProcessor<double,double,double,PeakHoldAveraging>;
      break;
    default:
      throw IndexOutOfBoundException("method",
                                     method,
                                     0,
                                     4,
                                     "PeakHoldAveragedSpectrumProcessor(TimeAveragingMethod method)",
                                     __FILE__, 
                                     __LINE__);
      break;
  }
}

//##ModelId=43E232D0028E
PeakHoldAveragedSpectrumProcessor::~PeakHoldAveragedSpectrumProcessor()
{
  if (_processor)
    delete _processor;
}

//##ModelId=43E232D0028F
size_t PeakHoldAveragedSpectrumProcessor::getNbMissingSpectrum() const
{
  return(_processor->getMissingInput());
}

//##ModelId=43E232D0029E
void PeakHoldAveragedSpectrumProcessor::restart()
{
  _processor->restart();
  _firstTime = true;
}

//##ModelId=43E232D002AD
void PeakHoldAveragedSpectrumProcessor::initAverageProcessor()
{
  _processor->init(_nbBins, _timeConstant);
}

}



