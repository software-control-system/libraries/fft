#include "PeakHoldAveraging.hpp"

namespace FFT {

//##ModelId=43E23464021C
PeakHoldAveraging::PeakHoldAveraging()
{
}

//##ModelId=43E234660162
PeakHoldAveraging::~PeakHoldAveraging()
{
}

//##ModelId=43DF2B9101BB
const double* PeakHoldAveraging::process(const double* newData, double weight, double lastDataWeight)
{
  // The PeakHoldAveraging does not make use of the weights given by AverageProcessor and its derived class
  // it just computes a max
  
  // set weights to 0 to get rid of 'unused parameter' warning with g++
  weight = 0;
  lastDataWeight = 0;

  for (size_t i = 0; i < _nbSamples; i++)
    _last[i] = (newData[i] < _last[i]) ? _last[i] : newData[i];

  return((const double*)_last);
}

}


