#include "PeakHoldMagnitudeAveragedSpectrumProcessor.hpp"
#include "Tools.hpp"
#include "Spectrum.hpp"

namespace FFT {

//##ModelId=43E233C60064
PeakHoldMagnitudeAveragedSpectrumProcessor::PeakHoldMagnitudeAveragedSpectrumProcessor(TimeAveragingMethod method)
: PeakHoldAveragedSpectrumProcessor(method)
{
}


//##ModelId=43E233C60074
PeakHoldMagnitudeAveragedSpectrumProcessor::~PeakHoldMagnitudeAveragedSpectrumProcessor()
{
}

//##ModelId=43E233C60075
bool PeakHoldMagnitudeAveragedSpectrumProcessor::process(const Spectrum& fftSpectrumData)
{
  this->firstTimeInit(fftSpectrumData);
  double* rmsValue = Tools::computeNorm(fftSpectrumData.getComplexData(), _nbBins);
  
  bool ready = _processor->process(rmsValue);
  delete [] rmsValue;
  if (ready)
  {
    memcpy(_availableData->getRealData(), _processor->getOutput(), _nbBins * sizeof(double));
  }
  return(ready);
}

}

