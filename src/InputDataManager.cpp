#include "InputDataManager.hpp"
#include <string.h> // for memcpy & memmove
#include "fftw3.h"

namespace FFT {

/**
 *  \brief Constructor
 */

//##ModelId=43D8F68D01E8
InputDataManager::InputDataManager() :
  _inputBlock(0L),
  _blockSize(0),
  _overlapingSamples(0),
  _cursor(0)
{
}



/**
 *  \brief Destructor
 */

//##ModelId=43D8FB9803D7
InputDataManager::~InputDataManager()
{
  reset();
}



/**
 *  \brief Stores some input samples
 *         and return a boolean indicating if enough samples have been provided
 *
 *  \param[in] inputData a pointer to a table of samples
 *  \param[in] n the number of samples pointed to by inputData
 *  \exception NullPointerException if inputData is NULL or if init() has not yet been called
 */

//##ModelId=43C7AF6503C7
bool InputDataManager::sendInput(const double* inputData, size_t n) throw(NullPointerException)
{
  if (!inputData)
    throw NullPointerException("inputData",
                               "InputDataManager::sendInput(const double* inputData, size_t n)",
                               __FILE__,
                               __LINE__);

  if (!_inputBlock)
    throw NullPointerException("_inputBlock",
                               "InputDataManager::sendInput(const double* inputData, size_t n)",
                               __FILE__,
                               __LINE__);

  size_t nbSamples = n < getNbMissingSamples() ? n : getNbMissingSamples();
  memcpy(_inputBlock + _cursor, inputData, nbSamples * sizeof(double));
  _cursor += nbSamples;

  return(getNbMissingSamples() == 0);
}



/**
 *  \brief Returns the stored block of samples
 *
 *  \exception NullPointerException if init() has not yet been called
 */

//##ModelId=43C7AFBE02E9
const double* InputDataManager::getInputBlock() const throw(NullPointerException)
{
  if (!_inputBlock)
    throw NullPointerException("_inputBlock",
                               "InputDataManager::getInputBlock()",
                               __FILE__,
                               __LINE__);
  return(_inputBlock);
}


/**
 *  \brief Put the instance in a state similar than it was just after creation
 */

//##ModelId=43CB6D6A03CE
void InputDataManager::reset()
{
  if (_inputBlock)
    fftw_free(_inputBlock);
  _inputBlock = 0L;
  _blockSize = 0;
  _overlapingSamples = 0;
  _cursor = 0;
}



/**
 *  \brief Initialize the instance
 *
 *  \param[in] blockSize the number of samples \f$ N \f$
 *  \param[in] overlapPercent the percentage of overlaping samples between two computations
 */

//##ModelId=43CE5D05002D
void InputDataManager::init(size_t blockSize, double overlapPercent)
{
  if (_inputBlock)
    fftw_free(_inputBlock);
  _inputBlock = (double*)fftw_malloc(sizeof(double) * blockSize);

  _blockSize = blockSize;
  _overlapingSamples = (int)(overlapPercent * (double)blockSize / 100);
}



/**
 *  \brief Return the number of samples needed to have a full block of input samples
 */

//##ModelId=43CF6C5801B9
size_t InputDataManager::getNbMissingSamples() const
{
  return(_blockSize - _cursor);
}

/**
 *  \brief Prepare for the acquisition of the next block
 *
 *  This function handles the overlaping region that needs to be kept between 2
 *  consecutive FFT computations
 *  
 *  \exception NullPointerException if init() has not been called yet
 */

//##ModelId=43CF6D93008A
void InputDataManager::beginNextBlockAquisition() throw(NullPointerException)
{
  if (!_inputBlock)
    throw NullPointerException("_inputBlock",
                               "InputDataManager::beginNextBlockAquisition()",
                               __FILE__,
                               __LINE__);

  // move the last _overlapingSamples 
  memmove(_inputBlock,
          _inputBlock + _blockSize - _overlapingSamples,
          _overlapingSamples * sizeof(double));
  _cursor = _overlapingSamples;

}

/**
 *  \brief Restart the storing of samples from the beginning, keeping the initialization parameters
 */

//##ModelId=43DE0F3D03B9
void InputDataManager::restart()
{
  _cursor = 0;
}

}

