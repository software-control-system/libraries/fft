#include <iostream>
#include "Spectrum.hpp"
#include "fftw3.h"
#include <yat/utils/Logging.h>

namespace FFT {

//##ModelId=43FC3FB802F2
Spectrum::Spectrum(ScalarType type, size_t size, const WindowInfo& windowInfo, double fs, double df, size_t fftSize)
: _windowInfo(windowInfo)
{
  if (type == REAL)
    _data = fftw_malloc(sizeof(double) * size);
  else
    _data = fftw_malloc(sizeof(std::complex<double>) * size);

  _size     = size;
  _type     = type;
  _fs       = fs;
  _df       = df;
  _fftSize  = fftSize;
}

//##ModelId=43FC3FB802F4
Spectrum::~Spectrum()
{
YAT_INFO << "\t\t\tSpectrum::~Spectrum()..." << std::endl;
  if (_data)
    fftw_free(_data);
YAT_INFO << "\t\t\tSpectrum::~Spectrum(): free data DONE!" << std::endl;
  _data = 0L;
  _size = 0;
  _type = REAL;
}

//##ModelId=43FC3FB80303
ScalarType Spectrum::getType() const
{
  return(_type);
}

//##ModelId=43FC3FB80321
const double* Spectrum::getRealData() const throw(NoSuchElementException)
{
  return((const double*)_data);
}

//##ModelId=43FC3FB80325
double* Spectrum::getRealData() throw(NoSuchElementException)
{
  return((double*)_data);
}

//##ModelId=43FC3FB80323
const std::complex<double>* Spectrum::getComplexData() const throw(NoSuchElementException)
{
  return((const std::complex<double>*)_data);
}

//##ModelId=43FC3FB80331
std::complex<double>* Spectrum::getComplexData() throw(NoSuchElementException)
{
  return((std::complex<double>*)_data);
}

//##ModelId=43FC3FB80302
size_t Spectrum::getNbBins() const
{
  return(_size);
}

//##ModelId=43FC41260322
double Spectrum::getSamplingFrequency() const
{
  return(_fs);
}

//##ModelId=43FC4144034E
double Spectrum::getResolution() const
{
  return(_df);
}

//##ModelId=43FC414F02A8
const WindowInfo& Spectrum::getWindowInfo() const
{
  return((const WindowInfo&)_windowInfo);
}

}
//##ModelId=43FDBDD0030A
size_t FFT::Spectrum::getWindowSize() const
{
  return(_windowInfo.getSize());
}

//##ModelId=43FDBDE302AA
size_t FFT::Spectrum::getFFTSize() const
{
  return(_fftSize);
}

