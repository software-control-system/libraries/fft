#include "PeakHoldPowerAveragedSpectrumProcessor.hpp"
#include "Tools.hpp"
#include "Spectrum.hpp"

namespace FFT {

//##ModelId=43E233E60395
PeakHoldPowerAveragedSpectrumProcessor::PeakHoldPowerAveragedSpectrumProcessor(TimeAveragingMethod method)
: PeakHoldAveragedSpectrumProcessor(method)
{
}


//##ModelId=43E233E60397
PeakHoldPowerAveragedSpectrumProcessor::~PeakHoldPowerAveragedSpectrumProcessor()
{
}

//##ModelId=43E233E603A5
bool PeakHoldPowerAveragedSpectrumProcessor::process(const Spectrum& fftSpectrumData)
{
  this->firstTimeInit(fftSpectrumData);
  double* rmsValue = Tools::computeSquareNorm(fftSpectrumData.getComplexData(), _nbBins);
  
  // scale
  unsigned int i;
  for (i = 0; i < _nbBins; i++)
    rmsValue[i] /= fftSpectrumData.getWindowSize();

  bool ready = _processor->process(rmsValue);
  delete [] rmsValue;
  if (ready)
    memcpy(_availableData->getRealData(), _processor->getOutput(), _nbBins * sizeof(double));

  return(ready);
}

}

