#include <iostream>
#include <string.h>
#include "FFTProcessor.hpp"
#include "FFTProcessorConfig.hpp"
#include "Constant.h"
#include "Spectrum.hpp"
#include <yat/utils/Logging.h>

namespace FFT {


/**
 *  \brief Constructor
 */

//##ModelId=43D8F6780039
FFTProcessor::FFTProcessor() :
  _config(0L),
  _spectrumReady(false),
  _availableFFTData(0L),
  _inputData(0L)
{
  ::memset(&_plan, 0, sizeof(fftw_plan));
}

/**
 *  \brief Destructor
 */

//##ModelId=43D8F67F01E8
FFTProcessor::~FFTProcessor()
{
  reset();
}

/**
 *  \brief Store some input data and if enough are available, compute the FFT
 * 
 *  This method stores the samples pointed by the input parameters until enough
 *  have been stored to compute a single FFT.
 *  In the case enough samples have been stored to compute a FFT,
 *  the data are taken from #_inputDataManager, and retrieved into #_inputData,
 *  then the windowing is applied, and the FFT is computed from the windowed signal
 * 
 *  Let
 *   - \f$N_{fft}\f$ be the FFT size
 *   - \f$N_w\f$ be the window size (the number of input samples)
 *   - \f$x_k\f$ be the input samples for \f$k \in [0, N-1]\f$
 *   - \f$w_k\f$ be the window values
 * 
 *  The output data is an array of \f$\frac{N}{2} + 1\f$ complex values.
 *  For \f$j \in [0, \frac{N}{2}]\f$
 * 
 *  \f[
 *     FFT_j = \frac{\sqrt{2}}{N_{fft}} \sum_{k = 0}^{N_w-1} x_k w_k e^{- 2 \pi \imath \frac{j k}{N_{fft}}} 
 *  \f]
 * 
 *  \param[in] inputData a pointer to the buffer containing input data
 *  \param[in] n the number of samples contained in the data
 *  \exception NullPointerException if inputData equals NULL
 *  \return A boolean indicating if the output data is ready
 */

//##ModelId=43CE4F800054
bool FFTProcessor::process(const double* inputData, size_t n) throw(NullPointerException)
{
  if (!inputData)
    throw NullPointerException("inputData",
                               "FFTProcessor::process(const double* inputData, size_t n)",
                               __FILE__,
                               __LINE__);

  // if spectrum has been computed last time, prepare for next block (handle overlap)
  if (_spectrumReady)
  {
    _inputDataManager.beginNextBlockAquisition();
    _spectrumReady = false;
  }

  // send input data and check if fft can be computed
  bool ready = _inputDataManager.sendInput(inputData,n);
  
  if (ready)
  {
    unsigned int i = 0;
    size_t N = _config->getWindowSize();
    ::memcpy(_inputData, _inputDataManager.getInputBlock(), N * sizeof(double));
    const double* _windowData = _config->getWindow().getValues();
    for (i = 0; i < N; i++)
      _inputData[i] *= _windowData[i];

    // compute FFT
    ::fftw_execute(_plan);
    
    // normalize the FFT
    std::complex<double>* fft = _availableFFTData->getComplexData();
    size_t nbbins = _availableFFTData->getNbBins();
    
    // convert to a one-sided spectrum
    double conversionFactor = MATH_CONSTANT::SQRT2 / N;
    for (i = 1; i < nbbins; i++)
      fft[i] *= conversionFactor;

    _spectrumReady = true;
  }
  return _spectrumReady;
}

/**
 *  \brief Put the instance in the same state that it was just after creation
 */

//##ModelId=43CE4F800057
void FFTProcessor::reset()
{
YAT_INFO << "\t\tFFTProcessor::reset _availableFFTData..." << std::endl;
  if (_availableFFTData)
    delete _availableFFTData;
YAT_INFO << "\t\tFFTProcessor::reset _availableFFTData DONE..." << std::endl;
  _availableFFTData = 0L;
  if (_inputData)
    ::fftw_free(_inputData);
YAT_INFO << "\t\tFFTProcessor::reset _inputData DONE.." << std::endl;
  _inputData = 0L;
  _inputDataManager.reset();
YAT_INFO << "\t\tFFTProcessor::reset _inputDataMgr DONE.." << std::endl;
  _config = 0L;
  _spectrumReady = false;
  ::fftw_destroy_plan(_plan);
YAT_INFO << "\t\tFFTProcessor::reset _plan DONE.." << std::endl;
  ::memset(&_plan, 0, sizeof(fftw_plan));
YAT_INFO << "\t\tFFTProcessor::reset DONE!" << std::endl;
}

/**
 *  \brief Pass a configuration to the processor,
 *         allocate memory and prepare FFT computations
 * 
 *  \param config a reference to configuration instance
 */

//##ModelId=43CE5D5B010C
void FFTProcessor::setConfig(FFTProcessorConfig& config)
{
YAT_INFO << "\t\tFFTProcessor::setConfig..." << std::endl;
  reset();
YAT_INFO << "\t\tFFTProcessor::setConfig -> reset done..." << std::endl;

  _config = &config;
  size_t windowSize = _config->getWindowSize();
  size_t fftSize    = _config->getFFTSize();


  _inputDataManager.init(windowSize, _config->getOverlapPercent());
YAT_INFO << "\t\tFFTProcessor::setConfig -> inputDataManager INIT done..." << std::endl;

  // allocate the input & output  block
  _inputData = (double*)::fftw_malloc(fftSize * sizeof(double));
YAT_INFO << "\t\tFFTProcessor::setConfig -> inputData allocated done..." << std::endl;
  size_t i;
  for (i = 0; i < fftSize; i++)
    _inputData[i] = 0.f;

YAT_INFO << "\t\tFFTProcessor::setConfig -> inputData initialised to 0..." << std::endl;
  _availableFFTData = new Spectrum(COMPLEX_INTERLEAVED,
                                   fftSize / 2 + 1,
                                   config.getWindow().getWindowInfo(),
                                   config.getSamplingFrequency(),
                                   config.getResolution(),
                                   fftSize);
YAT_INFO << "\t\tFFTProcessor::setConfig -> _availableFFTData created..." << std::endl;

  fftw_complex* out = reinterpret_cast<fftw_complex*>(_availableFFTData->getComplexData());
YAT_INFO << "\t\tFFTProcessor::setConfig -> fftw_complex* out cast..." << std::endl;

  // prepare FFT
  _plan = ::fftw_plan_dft_r2c_1d( fftSize, 
                                _inputData,
                                out,
                                FFTW_ESTIMATE);
YAT_INFO << "\t\tFFTProcessor::setConfig -> FINISHED." << std::endl;
}

/**
 *  \brief Return the data available in output
 */

//##ModelId=43CBBADF020E
const Spectrum& FFTProcessor::getData() const
{
  if (!_availableFFTData)
    throw NullPointerException("_availableFFTData",
                               "FFTProcessor::getData()",
                               __FILE__,
                               __LINE__);

  return((const Spectrum&)*_availableFFTData);
}

/**
 *  \brief Return the number of samples still needed to compute the next FFT
 */

//##ModelId=43CF72280127
size_t FFTProcessor::getNbMissingSamples() const
{
  return(_inputDataManager.getNbMissingSamples());
}

/**
 *  \brief Clear all previously stored data but keep the configuration on
 */

//##ModelId=43DE0EEA02E3
void FFTProcessor::restart()
{
  _inputDataManager.restart();
  _spectrumReady = false;
}

}

