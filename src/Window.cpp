#include "Window.hpp"
#include <math.h> // for cos()
#include "Constant.h" // for PI


namespace FFT {

/**
 *  \brief Constructor
 * 
 *  \param[in] type the type of the window
 *  \param[in] N the number of samples to generate
 */

//##ModelId=43D4F2500082
Window::Window(WindowType type, size_t N)
{
  // TODO check params, throw ex when N not even & windowtype invalid

  _windowInfo = new WindowInfo(type, N);
  _values = new double[N];
  double temp2 = 2.0 * MATH_CONSTANT::PI / N;


  double scaleFactor = 1 / _windowInfo->getCoherentGain();
  unsigned int i;
  for (i = 0; i < N; ++i)
  {
    std::vector<double>::size_type j;
    double cosparam = 0;
    _values[i] = 0;
    for(j = 0; j < _windowInfo->getNbCoeffs(); j++)
    {
      double coeff = _windowInfo->getCoeff(j);
      if (j & 0x1)
        coeff = -coeff;
      
      _values[i] += scaleFactor * coeff * cos(cosparam * i);

      cosparam += temp2;
    }
  }
}


/**
 *  \brief Destructor
 */

//##ModelId=43D8A46303B5
Window::~Window()
{
  if (_values)
    delete [] _values;
  if (_windowInfo)
    delete _windowInfo;
}




/**
 *  \brief Return a buffer containing the sample values of the current instance
 */

//##ModelId=43D4F26A00A4
const double* Window::getValues() const
{
  return((const double*)_values);
}


//##ModelId=43FC38FA0087
const WindowInfo& FFT::Window::getWindowInfo() const
{
  return(*_windowInfo);
}


//##ModelId=43FC39AB0167
WindowType FFT::Window::getWindowType() const
{
  return(_windowInfo->getWindowType());
}

}
