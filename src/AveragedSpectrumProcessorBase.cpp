#include "AveragedSpectrumProcessorBase.hpp"

namespace FFT {

//##ModelId=43D5F9460051
AveragedSpectrumProcessorBase::AveragedSpectrumProcessorBase()
: _timeConstant(1),
  _firstTime(true),
  _outputType(REAL),
  _nbBins(0)
{
}

//##ModelId=43DF949A0133
AveragedSpectrumProcessorBase::~AveragedSpectrumProcessorBase()
{
}

//##ModelId=43CE52910394
void AveragedSpectrumProcessorBase::init(unsigned int timeConstant)
{
  _timeConstant = timeConstant;
  this->initAverageProcessor();
}


//##ModelId=43E1FF23011D
void AveragedSpectrumProcessorBase::firstTimeInit(const Spectrum& fftSpectrumData)
{
  if (_firstTime == true)
  {
    _nbBins = fftSpectrumData.getNbBins();
    this->initAverageProcessor();
    
    if (_availableData)
      delete _availableData;
    _availableData = new Spectrum(_outputType,
                                  _nbBins, 
                                  fftSpectrumData.getWindowInfo(),
                                  fftSpectrumData.getSamplingFrequency(), 
                                  fftSpectrumData.getResolution(),
                                  fftSpectrumData.getFFTSize());

    _firstTime = false;
  }
}

}

