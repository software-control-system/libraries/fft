#include "WindowInfo.hpp"


namespace FFT
{

//##ModelId=43FB44EC0361
WindowInfo::WindowInfo(WindowType type, size_t size)
{
  _type = type;
  _size = size;

  switch(type)
  {
  case RECTANGULAR:
    {
      _coeffs.resize(1);
      _coeffs[0] = 1;

      _coherentGain = 1.;
      _enbw = 1.;
      _6dB_BW = 1.21;
      break;
    }
  case BLACKMAN:
    {
      _coeffs.resize(3);
      _coeffs[0] = 0.42;
      _coeffs[1] = 0.5;
      _coeffs[2] = 0.08;

      _coherentGain = 0.42;
      _enbw = 1.726757;
      _6dB_BW = 2.3;
      break;
    }
  case EXACT_BLACKMAN:
    {
      _coeffs.resize(3);
      _coeffs[0] = 0.42659071367153911200;
      _coeffs[1] = 0.49656061908856408100;
      _coeffs[2] = 0.07684866723989682010;

      _coherentGain = 0.42659071367153911200;
      _enbw = 1.693699;
      _6dB_BW = 2.25;
      break;
    }
  case HAMMING:
    {
      _coeffs.resize(2);
      _coeffs[0] = 0.54;
      _coeffs[1] = 0.46;

      _coherentGain = 0.54;
      _enbw = 1.362826;
      _6dB_BW = 1.82;
      break;
    }
  case HANN:
    {
      _coeffs.resize(2);
      _coeffs[0] = 0.5;
      _coeffs[1] = 0.5;

      _coherentGain = 0.5;
      _enbw = 1.5;
      _6dB_BW = 2.0;
      break;
    }
  case FLATTOP:
    {
      _coeffs.resize(5);
      _coeffs[0] = 0.215578948;
      _coeffs[1] = 0.41663158;
      _coeffs[2] = 0.277263158;
      _coeffs[3] = 0.083578947;
      _coeffs[4] = 0.006947368;

      _coherentGain = 0.215578948;
      _enbw = 3.770246506303;
      _6dB_BW = 4.58;
      break;
    }
  case BLACKMAN_HARRIS_3:
    {
      _coeffs.resize(3);
      _coeffs[0] = 0.42323;
      _coeffs[1] = 0.49755;
      _coeffs[2] = 0.07922;

      _coherentGain = 0.42323;
      _enbw = 1.0708538;
      _6dB_BW = 2.27;
      break;
    }
  case BLACKMAN_HARRIS_4:
    {
      _coeffs.resize(4);
      _coeffs[0] = 0.35875;
      _coeffs[1] = 0.48829;
      _coeffs[2] = 0.14128;
      _coeffs[3] = 0.01168;

      _coherentGain = 0.35875;
      _enbw = 2.004353;
      _6dB_BW = 2.67;
      break;
    }
  case BLACKMAN_HARRIS_7:
    {
      _coeffs.resize(7);
      _coeffs[0] = 0.27105140069342415;
      _coeffs[1] = 0.43329793923448606;
      _coeffs[2] = 0.21812299954311062;
      _coeffs[3] = 0.065925446388030898;
      _coeffs[4] = 0.010811742098372268;
      _coeffs[5] = 7.7658482522509342E-4;
      _coeffs[6] = 1.3887217350903198E-5;
      
      _coherentGain = 0.27105140069342415;
      _enbw = 2.631905;
      _6dB_BW = 3.5;
      break;
    }
  case LOW_SIDELOBE:
    {
      _coeffs.resize(5);
      _coeffs[0] = 0.323215218;
      _coeffs[1] = 0.471492057;
      _coeffs[2] = 0.17553428;
      _coeffs[3] = 0.028497078;
      _coeffs[4] = 0.001261367;
      
      _coherentGain = 0.323215218;
      _enbw = 2.215350782519;
      _6dB_BW = 2.95;
      break;
    }
  }
}
    
//##ModelId=43FB44EC0371
WindowInfo::~WindowInfo()
{
}

//##ModelId=43FB44EC0372
WindowType WindowInfo::getWindowType() const
{
  return(_type);
}

//##ModelId=43FB44EC0380
size_t WindowInfo::getSize() const
{
  return(_size);
}

//##ModelId=43FB44EC0390
double WindowInfo::getENBW() const
{
  return(_enbw);
}

//##ModelId=43FB44EC0391
double WindowInfo::getCoherentGain() const
{
  return(_coherentGain);
}

//##ModelId=43FB44EC0392
double WindowInfo::get6dB_BW() const
{
  return(_6dB_BW);
}

//##ModelId=43FC3500026A
size_t WindowInfo::getNbCoeffs() const
{
  return(_coeffs.size());
}

//##ModelId=43FC354101A4
double WindowInfo::getCoeff(size_t i) const
{
  return(_coeffs[i]);
}


//##ModelId=43FC630100E2
WindowInfo::WindowInfo(const WindowInfo& right)
{
  _type = right.getWindowType();
  _size = right.getSize();
  _enbw = right.getENBW();
  _coherentGain = right.getCoherentGain();
  _6dB_BW = right.get6dB_BW();

  _coeffs.resize(right.getNbCoeffs());
  std::vector<double>::size_type i;
  for (i = 0; i < _coeffs.size(); i++)
    _coeffs[i] = right.getCoeff(i);
}

}
