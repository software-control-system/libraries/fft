#include "VectorMagnitudeAveragedSpectrumProcessor.hpp"
#include "Tools.hpp"
#include "Spectrum.hpp"

namespace FFT {

//##ModelId=43E1DBB302D6
VectorMagnitudeAveragedSpectrumProcessor::VectorMagnitudeAveragedSpectrumProcessor(TimeAveragingMethod method)
: VectorAveragedSpectrumProcessor(method)
{
  _outputType = COMPLEX_INTERLEAVED;
}


//##ModelId=43E1DBB302E6
VectorMagnitudeAveragedSpectrumProcessor::~VectorMagnitudeAveragedSpectrumProcessor()
{
}

//##ModelId=43E1DBB302E7
bool VectorMagnitudeAveragedSpectrumProcessor::process(const Spectrum& fftSpectrumData)
{
  this->firstTimeInit(fftSpectrumData);
  bool ready = _processor->process(fftSpectrumData.getComplexData());

  if (ready)
  {
    const std::complex<double>* in = _processor->getOutput();
    std::complex<double>* out = _availableData->getComplexData();
    memcpy(out, in, _nbBins * sizeof(std::complex<double>));
  }
  return(ready);
}

}

