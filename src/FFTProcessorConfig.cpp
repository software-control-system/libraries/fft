#include "FFTProcessorConfig.hpp"
#include <math.h> // for ceil
#include <limits> // for std::numeric_limits<double>::max()
#include <iostream>

namespace FFT {

/**
 *  \brief Constructor
 */

//##ModelId=43D9F5FC0339
FFTProcessorConfig::FFTProcessorConfig() :
  _samplingFrequency(0),
  _resolutionBandWidth(0),
  _window(0L),
  _windowSize(0),
  _overlapPercent(0)
{
}



/**
 *  \brief Destructor
 */

//##ModelId=43D9F603009A
FFTProcessorConfig::~FFTProcessorConfig()
{
  if (_window)
    delete _window;
}



/**
 *  \brief Initialize a configuration instance by providing all the main parameters
 *
 *  The aim of this function is to set the frequency axis parameters and then
 *  to compute :
 *    - the input block size, which is equal to the number of samples 
 *      to aquire to have a FFT with the desired parameters (= the window size)
 *    - the FFT size, which is the size on which the FFT is computed,
 *      and which will be related to the output number of points
 *
 *  It is possible to zero-pad the input samples to artificially increase the FFT resolution
 *
 *  Let 
 *   - \f$df_u\f$ be the resolution bandwidth desired by the user
 *   - \f$df\f$ be the effective resolution bandwidth
 *   - \f$f_s\f$ be the sampling frequency
 *   - \f$N_w\f$ be the window size (or number of input samples necessary)
 *   - \f$N_{fft}\f$ be the fft size
 *   - \f$N_u\f$ be the number of spectral lines desired by the user
 *
 *  The strategy used here is :
 *   -# compute \f$N_w\f$
 *   -# compute \f$N_{fft}\f$ by \f$ N_{fft} = \max (N_u, N_w)\f$
 *
 *  If you want to skip the 2nd step, you just have to set \f$ N_u \f$ to 0 and you will get \f$ N_{fft} = N_w \f$
 *
 *  To compute \f$N_w\f$, we start with the following relation : \f$ df = \frac{f_s}{N_w} \f$
 *  and \f$N_w\f$ must be a power of 2 so as FFT computation be fast enough
 *
 *  To achieve that, first \f$N_w\f$ is computed to fit a power of 2
 *  according to the following formula :
 *  \f[ N_w = 2^{\left\lceil\log_2\frac{f_s}{df_u}\right\rceil} \f]
 *
 *  Then \f$N_{fft}\f$ is computed by \f$ N_{fft} = \max (N_u, N_w)\f$
 *
 *  Finally the effective resolution bandwidth is updated through :
 *  \f$ df = \frac{f_s}{N_{fft}}  \f$
 *
 *  This insures that \f$ df < df_u \f$ and that \f$ N_{fft} \geq N_u\f$
 *
 *  \param[in] samplingFrequency the sampling frequency
 *  \param[in] desiredRBW the minimal resolution bandwidth desired by the user
 *  \param[in] windowType the type of window to be applied on the signal
 *  \param[in] overlapPercent the percentage of overlaping samples
 *  \param[in] minimalSpectralLines the minimal number of frequency bins in the FFT
 *
 *  \exception ValueOutOfBoundException if \f$f_s < 0\f$ or if \f$df \notin [0, f_s]\f$
 *  \exception ValueOutOfBoundException if \f$overlap \notin [0, 100]\f$
 */

//##ModelId=43CD132A021F
void FFTProcessorConfig::init(double samplingFrequency, double desiredRBW, WindowType windowType, double overlapPercent, size_t minimalSpectralLines) throw(ValueOutOfBoundException)
{
  

  if (samplingFrequency <= 0.f)
    throw ValueOutOfBoundException("samplingFrequency",
                                    samplingFrequency,
                                    0.f, 
                                    std::numeric_limits<double>::max(), 
                                    "FFTProcessorConfig::setFrequencyAxisParams(double samplingFrequency, double desiredRBW)", 
                                    __FILE__, 
                                    __LINE__);

  if ((desiredRBW <= 0.f) || (desiredRBW >= samplingFrequency))
    throw ValueOutOfBoundException("desiredRBW",
                                    desiredRBW,
                                    0.f, 
                                    samplingFrequency, 
                                    "FFTProcessorConfig::setFrequencyAxisParams(double samplingFrequency, double desiredRBW)", 
                                    __FILE__, 
                                    __LINE__);

  if ((overlapPercent >= 0.f) && (overlapPercent < 100.f))
    _overlapPercent = overlapPercent;
  else
    throw ValueOutOfBoundException("overlap",
                                   overlapPercent,
                                   0.f,
                                   100.f,
                                   "FFTProcessorConfig::setOverlapPercent(double overlap)",
                                   __FILE__,
                                   __LINE__);


  
  double _blockSizeEstim = samplingFrequency / desiredRBW;
  
  // ceil to the next closest power of 2
  // there is a diff of behavior between LINUX and WIN32, on Linux : ceil(10.0) = 11, on Windows, ceil(10.0) = 10
  double p1 = (log(_blockSizeEstim) / log(2.f)) == floor(log(_blockSizeEstim) / log(2.f)) ? 
                        (log(_blockSizeEstim) / log(2.f)) :
                        ceil(log(_blockSizeEstim) / log(2.f));
  _windowSize = (size_t)pow(2,p1);

  if ( (minimalSpectralLines & (minimalSpectralLines - 1)) != 0 )
  {
    double p2 = (log((double)minimalSpectralLines) / log(2.f)) == floor(log((double)minimalSpectralLines) / log(2.f)) ? 
                          (log((double)minimalSpectralLines) / log(2.f)) :
                          ceil(log((double)minimalSpectralLines) / log(2.f));
    minimalSpectralLines = (size_t)pow(2,p2);
   
    
  }
  // choose the maximum between spectralLines and resolution
  _fftSize = (_windowSize < minimalSpectralLines) ? minimalSpectralLines : _windowSize;

  _samplingFrequency = samplingFrequency;
  _resolutionBandWidth = _samplingFrequency / (double)_fftSize;
  
  // force an update of the window because the blockSize may have changed
  if (_window)
    delete _window;
  _window = new Window(windowType, _windowSize);
  
}


/**
 *  \brief Return the effective resolution bandwidth of the computed FFT
 *
 */

//##ModelId=43CD132A023E
double FFTProcessorConfig::getResolution() const
{
  return(_resolutionBandWidth);
}

/**
 *  \brief Return the sampling frequency
 *
 */

//##ModelId=43D1105A03AC
double FFTProcessorConfig::getSamplingFrequency() const
{
  return(_samplingFrequency);
}

/**
 *  \brief Return the number of samples used to compute the FFT
 *
 */

//##ModelId=43CD132A0240
size_t FFTProcessorConfig::getWindowSize() const
{
  return(_windowSize);
}

/**
 *  \brief Return the number of overlaping samples used to compute two consecutive FFT
 *
 */

//##ModelId=43CF6AD801B3
double FFTProcessorConfig::getOverlapPercent() const
{
  return(_overlapPercent);
}

/**
 *  \brief Return a reference to the window used for FFT computation
 *
 *  \attention You must have called at least initialize(), setWindow(),
 *             or setFrequencyAxisParams() before calling this method
 *
 *  \exception NullPointerException if the window has not been set yet
 *  \sa initialize(), setWindow(), and setFrequencyAxisParams()
 */

//##ModelId=43D1109A009B
const Window& FFTProcessorConfig::getWindow() const throw(NullPointerException)
{
  if (!_window)
    throw NullPointerException("_window",
                               "FFTProcessorConfig::getWindow()",
                               __FILE__,
                               __LINE__);
  return((const Window&)(*_window));
}


//##ModelId=43FD9935003B
size_t FFTProcessorConfig::getFFTSize() const
{
  return(_fftSize);
}


}

