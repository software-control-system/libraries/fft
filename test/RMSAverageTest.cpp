#include "RMSAverageTest.hpp"

#include "RMSMagnitudeAveragedSpectrumProcessor.hpp"
#include "RMSPowerAveragedSpectrumProcessor.hpp"


using namespace FFT;


#pragma warning ( disable : 4786 )
CPPUNIT_TEST_SUITE_REGISTRATION( RMSAverageTest );

void RMSAverageTest::testMagnitude_ONE_SHOT() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new RMSMagnitudeAveragedSpectrumProcessor(ONE_SHOT);
  testAverageProcessor(*avProc, ONE_SHOT, "RMS_Magnitude_ONE_SHOT");
  delete avProc;
}

void RMSAverageTest::testMagnitude_AUTO_RESTART() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new RMSMagnitudeAveragedSpectrumProcessor(AUTO_RESTART);
  testAverageProcessor(*avProc, AUTO_RESTART, "RMS_Magnitude_AUTO_RESTART");
  delete avProc;
}

void RMSAverageTest::testMagnitude_MOVING_AVERAGE() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new RMSMagnitudeAveragedSpectrumProcessor(MOVING_AVERAGE);
  testAverageProcessor(*avProc, MOVING_AVERAGE, "RMS_Magnitude_MOVING_AVERAGE");
  delete avProc;
}


void RMSAverageTest::testMagnitude_LINEAR_CONTINUOUS() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new RMSMagnitudeAveragedSpectrumProcessor(LINEAR_CONTINUOUS);
  testAverageProcessor(*avProc, LINEAR_CONTINUOUS, "RMS_Magnitude_LINEAR_CONTINUOUS");
  delete avProc;
}


void RMSAverageTest::testMagnitude_EXPONENTIAL_CONTINUOUS() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new RMSMagnitudeAveragedSpectrumProcessor(EXPONENTIAL_CONTINUOUS);
  testAverageProcessor(*avProc, EXPONENTIAL_CONTINUOUS, "RMS_Magnitude_EXPONENTIAL_CONTINUOUS");
  delete avProc;
}

void RMSAverageTest::testPower_ONE_SHOT() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new RMSPowerAveragedSpectrumProcessor(ONE_SHOT);
  testAverageProcessor(*avProc, ONE_SHOT, "RMS_Power_ONE_SHOT");
  delete avProc;
}

void RMSAverageTest::testPower_AUTO_RESTART() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new RMSPowerAveragedSpectrumProcessor(AUTO_RESTART);
  testAverageProcessor(*avProc, AUTO_RESTART, "RMS_Power_AUTO_RESTART");
  delete avProc;
}

void RMSAverageTest::testPower_MOVING_AVERAGE() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new RMSPowerAveragedSpectrumProcessor(MOVING_AVERAGE);
  testAverageProcessor(*avProc, MOVING_AVERAGE, "RMS_Power_MOVING_AVERAGE");
  delete avProc;
}


void RMSAverageTest::testPower_LINEAR_CONTINUOUS() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new RMSPowerAveragedSpectrumProcessor(LINEAR_CONTINUOUS);
  testAverageProcessor(*avProc, LINEAR_CONTINUOUS, "RMS_Power_LINEAR_CONTINUOUS");
  delete avProc;
}


void RMSAverageTest::testPower_EXPONENTIAL_CONTINUOUS() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new RMSPowerAveragedSpectrumProcessor(EXPONENTIAL_CONTINUOUS);
  testAverageProcessor(*avProc, EXPONENTIAL_CONTINUOUS, "RMS_Power_EXPONENTIAL_CONTINUOUS");
  delete avProc;
}

