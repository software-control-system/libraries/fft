#include "AverageTest.hpp"
#include <float.h>
#include <stdlib.h>
#include <time.h>
#include "Common.hpp"

using namespace FFT;

const double PI = 3.1415926535897932;

void AverageTest::setUp()
{
  printf("\n\n");
}

void AverageTest::tearDown()
{
}

void AverageTest::testAverageProcessor(AveragedSpectrumProcessorBase& avProc, TimeAveragingMethod method, std::string outputFileBaseName)
{
  printf(outputFileBaseName.c_str());
  printf("\n");

  FFTProcessorConfig fftProcConfig;
  FFTProcessor fftProc;

  int i = 0;

  double fs = 64000.f;
  double rb = 64000.f / 1024.f;
  WindowType windowType = HANN;
  double overlap = 0.;
  int nbSpectrum = 64;
  int averagingDuration = 8;

  fftProcConfig.init(fs,rb,windowType,overlap,4096);
  fftProc.setConfig(fftProcConfig);

  avProc.init(averagingDuration);


  // Generate input data
  // -------------------------------------------------------
  size_t blockSize = fftProcConfig.getWindowSize();
  double* input = new double [blockSize];
  
  // Send input data
  // -------------------------------------------------------
  bool fftReady;
  bool averageReady;
  for (i = 0; i < nbSpectrum; i++)
  {
    createCosine(input, blockSize);
    fftReady = fftProc.process(input, blockSize);
    CPPUNIT_ASSERT(fftReady == true);

    averageReady = avProc.process(fftProc.getData());

    testAverageReady(method,i,averagingDuration,averageReady);
  

    if (averageReady)
    {
      char tmp[10];
      sprintf(tmp, "%.2d", i);
      ::saveOutputToFile(outputFileBaseName + "_" + tmp + ".dat", avProc.getData(),fs);

      printMax(avProc.getData());
    }
  }

  // Test restart
  // -------------------------------------------------------
  fftProc.restart();
  avProc.restart();

  for (i = 0; i < nbSpectrum; i++)
  {
    createCosine(input, blockSize);
    fftReady = fftProc.process(input, blockSize);
    CPPUNIT_ASSERT(fftReady == true);

    averageReady = avProc.process(fftProc.getData());

    testAverageReady(method,i,averagingDuration,averageReady);
  }

  delete [] input;
}

void AverageTest::testAverageReady(TimeAveragingMethod method, int spectrumIndice, int timeConstant, bool averageReady)
{

  switch (method)
  {
    case AUTO_RESTART:
      {
        if (((spectrumIndice + 1) % timeConstant) != 0)
          CPPUNIT_ASSERT(averageReady == false);
        if (((spectrumIndice + 1) % timeConstant) == 0)
          CPPUNIT_ASSERT(averageReady == true);
        break;
      }
    case ONE_SHOT:
    case MOVING_AVERAGE:
    case LINEAR_CONTINUOUS:
    case EXPONENTIAL_CONTINUOUS:
      {
        if (spectrumIndice < timeConstant - 1)
          CPPUNIT_ASSERT(averageReady == false);
        else if (spectrumIndice >=  timeConstant - 1)
          CPPUNIT_ASSERT(averageReady == true);
        break;
      }
    default:
      CPPUNIT_FAIL("wrong TimeAveragingMethod");
  }
}

void AverageTest::createCosine(double* input, int n)
{
  srand( (unsigned)time( NULL ) );

  double reducedFreq1 = .053;
//  double reducedFreq2 = .127;
  int i;
  
  
 
  for (i = 0; i < n; i++)
  {
    double randnum = (double)rand();
    randnum -= (RAND_MAX / 2); // randnum is in [-RAND_MAX/2 ; RAND_MAX/2]
    randnum /= (RAND_MAX / 2); // randnum is in [-1 ; 1]
    randnum /= 10;

    input[i] = 3 * cos (2 * PI * reducedFreq1 * i) + randnum;
  }

  /*
  for (i = 0; i < n; i++)
  {
    double term1 = cos (2 * PI * reducedFreq1 * i);
    double term2 = cos (2 * PI * reducedFreq2 * i);
    double modul1 = cos( i * PI / (2 * (n - 1)));
    modul1 *= modul1;
    double modul2 = sin( i * PI / (2 * (n - 1)));
    modul2 *= modul2;
    input[i] = term1 * modul1 + term2 * modul2;
  }
  */
}


void AverageTest::printMax(const FFT::Spectrum& data)
{
  // get max
  double max = 0;
  double indexMax = 0;
  unsigned int i;

  switch(data.getType())
  {
  case REAL:
    {
      const double* buf = data.getRealData();
      for(i = 0; i < data.getNbBins(); i++)
      {
        if (buf[i] > max)
        {
          max = buf[i];
          indexMax = i;
        }
      }
      break;
    }
  case COMPLEX_INTERLEAVED:
    {
      const std::complex<double>* buf = data.getComplexData();
      for(i = 0; i < data.getNbBins(); i++)
      {
        double rms = sqrt(norm(buf[i]));
        if (rms > max)
        {
          max = rms;
          indexMax = i;
        }
      }
      break;
    }
  default:
    CPPUNIT_FAIL("wrong data type");
  }

  printf("max : %f    ind : %f\n", max, indexMax / data.getNbBins());

}

