#include <cppunit/extensions/HelperMacros.h>


#include "FFTProcessor.hpp"
#include "FFTProcessorConfig.hpp"



class FFTProcessorTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE( FFTProcessorTest );
  CPPUNIT_TEST( testBasicFFT );
  CPPUNIT_TEST( testTwoTimesFFTWithoutOverlap );
  CPPUNIT_TEST( testTwoTimesFFTWithOverlap );
  CPPUNIT_TEST( testChangeConfig );
  CPPUNIT_TEST( testTooMuchInput );
  CPPUNIT_TEST( testReset );
  CPPUNIT_TEST_SUITE_END();



  public:

    void setUp();
    void tearDown();

    void testBasicFFT();
    void testTwoTimesFFTWithoutOverlap();
    void testTwoTimesFFTWithOverlap();
    void testChangeConfig();
    void testTooMuchInput();
    void testReset();


  private:

    double* createCosine(int n, double reducedFrequency);

  private:
    FFT::FFTProcessorConfig* _config;
    FFT::FFTProcessor* _processor;
};
