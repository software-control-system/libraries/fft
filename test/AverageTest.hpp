#include <cppunit/extensions/HelperMacros.h>

#include "FFTProcessor.hpp"
#include "FFTProcessorConfig.hpp"
#include "AveragedSpectrumProcessorBase.hpp"

class AverageTest : public CppUnit::TestFixture
{
  public:

    void setUp();
    void tearDown();

    virtual void testMagnitude_ONE_SHOT() = 0;
    virtual void testMagnitude_AUTO_RESTART() = 0;
    virtual void testMagnitude_MOVING_AVERAGE() = 0;
    virtual void testMagnitude_LINEAR_CONTINUOUS() = 0;
    virtual void testMagnitude_EXPONENTIAL_CONTINUOUS() = 0;

    virtual void testPower_ONE_SHOT() = 0;
    virtual void testPower_AUTO_RESTART() = 0;
    virtual void testPower_MOVING_AVERAGE() = 0;
    virtual void testPower_LINEAR_CONTINUOUS() = 0;
    virtual void testPower_EXPONENTIAL_CONTINUOUS() = 0;
    

  protected:

    void testAverageProcessor(FFT::AveragedSpectrumProcessorBase& avProc, FFT::TimeAveragingMethod method, std::string outputFileBaseName);
    void testAverageReady(FFT::TimeAveragingMethod method, int spectrumIndice, int timeConstant, bool averageReady);


  private:

    void createCosine(double* input,int n);
    void printMax(const FFT::Spectrum& data);
};

