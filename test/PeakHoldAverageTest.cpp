#include "PeakHoldAverageTest.hpp"

#include "PeakHoldMagnitudeAveragedSpectrumProcessor.hpp"
#include "PeakHoldPowerAveragedSpectrumProcessor.hpp"

using namespace FFT;


#pragma warning ( disable : 4786 )
CPPUNIT_TEST_SUITE_REGISTRATION( PeakHoldAverageTest );

void PeakHoldAverageTest::testMagnitude_ONE_SHOT() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new PeakHoldMagnitudeAveragedSpectrumProcessor(ONE_SHOT);
  testAverageProcessor(*avProc, ONE_SHOT, "PeakHold_Magnitude_ONE_SHOT");
  delete avProc;
}

void PeakHoldAverageTest::testMagnitude_AUTO_RESTART() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new PeakHoldMagnitudeAveragedSpectrumProcessor(AUTO_RESTART);
  testAverageProcessor(*avProc, AUTO_RESTART, "PeakHold_Magnitude_AUTO_RESTART");
  delete avProc;
}

void PeakHoldAverageTest::testMagnitude_MOVING_AVERAGE() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new PeakHoldMagnitudeAveragedSpectrumProcessor(MOVING_AVERAGE);
  testAverageProcessor(*avProc, MOVING_AVERAGE, "PeakHold_Magnitude_MOVING_AVERAGE");
  delete avProc;
}


void PeakHoldAverageTest::testMagnitude_LINEAR_CONTINUOUS() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new PeakHoldMagnitudeAveragedSpectrumProcessor(LINEAR_CONTINUOUS);
  testAverageProcessor(*avProc, LINEAR_CONTINUOUS, "PeakHold_Magnitude_LINEAR_CONTINUOUS");
  delete avProc;
}


void PeakHoldAverageTest::testMagnitude_EXPONENTIAL_CONTINUOUS() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new PeakHoldMagnitudeAveragedSpectrumProcessor(EXPONENTIAL_CONTINUOUS);
  testAverageProcessor(*avProc, EXPONENTIAL_CONTINUOUS, "PeakHold_Magnitude_EXPONENTIAL_CONTINUOUS");
  delete avProc;
}

void PeakHoldAverageTest::testPower_ONE_SHOT() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new PeakHoldPowerAveragedSpectrumProcessor(ONE_SHOT);
  testAverageProcessor(*avProc, ONE_SHOT, "PeakHold_Power_ONE_SHOT");
  delete avProc;
}

void PeakHoldAverageTest::testPower_AUTO_RESTART() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new PeakHoldPowerAveragedSpectrumProcessor(AUTO_RESTART);
  testAverageProcessor(*avProc, AUTO_RESTART, "PeakHold_Power_AUTO_RESTART");
  delete avProc;
}

void PeakHoldAverageTest::testPower_MOVING_AVERAGE() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new PeakHoldPowerAveragedSpectrumProcessor(MOVING_AVERAGE);
  testAverageProcessor(*avProc, MOVING_AVERAGE, "PeakHold_Power_MOVING_AVERAGE");
  delete avProc;
}


void PeakHoldAverageTest::testPower_LINEAR_CONTINUOUS() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new PeakHoldPowerAveragedSpectrumProcessor(LINEAR_CONTINUOUS);
  testAverageProcessor(*avProc, LINEAR_CONTINUOUS, "PeakHold_Power_LINEAR_CONTINUOUS");
  delete avProc;
}


void PeakHoldAverageTest::testPower_EXPONENTIAL_CONTINUOUS() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new PeakHoldPowerAveragedSpectrumProcessor(EXPONENTIAL_CONTINUOUS);
  testAverageProcessor(*avProc, EXPONENTIAL_CONTINUOUS, "PeakHold_Power_EXPONENTIAL_CONTINUOUS");
  delete avProc;
}

