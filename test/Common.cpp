#include <cppunit/extensions/HelperMacros.h>
#include "Common.hpp"
#include <stdio.h>


void saveOutputToFile(std::string filename, const FFT::Spectrum& data, double fs)
{
  FILE* outfile = fopen(filename.c_str(), "wb");

  unsigned int i;
  double n = 2 * (data.getNbBins() - 1);

  switch(data.getType())
  {
  case FFT::REAL:
    {
      const double* buf = data.getRealData();
      for(i = 0; i < data.getNbBins(); i++)
      {
        double temp = i * fs / n;
        fwrite(&temp, sizeof(double), 1, outfile);
        temp = buf[i];
        fwrite(&temp, sizeof(double), 1, outfile);
      }
      break;
    }
  case FFT::COMPLEX_INTERLEAVED:
    {
      const std::complex<double>* buf = data.getComplexData();
      for(i = 0; i < data.getNbBins(); i++)
      {
        double temp = i * fs / n;
        fwrite(&temp, sizeof(double), 1, outfile);
        temp = (double)sqrt(buf[i].real() * buf[i].real() + buf[i].imag() * buf[i].imag());
        fwrite(&temp, sizeof(double), 1, outfile);
      }
      break;
    }
  default:
    CPPUNIT_FAIL("wrong data type");
  }

  fclose(outfile);
  


}
