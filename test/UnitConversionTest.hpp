#include <cppunit/extensions/HelperMacros.h>


#include "FFTProcessor.hpp"
#include "FFTProcessorConfig.hpp"
#include "PowerSpectrumUnitConverter.hpp"
#include "RMSPowerAveragedSpectrumProcessor.hpp"


class UnitConversionTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE( UnitConversionTest );
  CPPUNIT_TEST( test_dBV );
  CPPUNIT_TEST( test_dBmW );
  CPPUNIT_TEST_SUITE_END();



  public:

    void setUp();
    void tearDown();



    void test_dBV();
    void test_dBmW();

  
  private:

    double* createInputSignal(int n);

    FFT::FFTProcessorConfig* _config;
    FFT::FFTProcessor* _fftProcessor;
    FFT::RMSPowerAveragedSpectrumProcessor* _powerProcessor;
    FFT::PowerSpectrumUnitConverter* _unitConv;
};

