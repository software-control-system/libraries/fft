#include "WindowTest.hpp"
#include "FFTProcessor.hpp"
#include "FFTProcessorConfig.hpp"
#include "RMSPowerAveragedSpectrumProcessor.hpp"
#include "Common.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION( WindowTest );

const double PI = 3.1415926535897932;

using namespace FFT;


void WindowTest::setUp()
{
}

void WindowTest::tearDown() 
{
}

void WindowTest::testWindow(WindowType type)
{
  // Initialize
  // -------------------------------------------------------
  double fs = 64000;
  double rb = 64000.f / 1024;
  double overlap = 0;

  FFTProcessorConfig* config = new FFTProcessorConfig();
  FFTProcessor* processor = new FFTProcessor();

  // do a precise FFT to see the lobes of the windows
  config->init(fs,rb,type,overlap,1024 * 8);

  processor->setConfig(*config);


  // Generate input data
  // -------------------------------------------------------
  size_t blockSize = config->getWindowSize();
  double* input = new double[blockSize];
  size_t i;
  for (i = 0; i < blockSize; i++)
    input[i] = cos (2 * PI * .2 * i);
  

  // Send input data
  // -------------------------------------------------------
  bool ready;
  ready = processor->process(input,blockSize);
  CPPUNIT_ASSERT(ready == true);
  CPPUNIT_ASSERT(processor->getNbMissingSamples() == 0);


  // Get FFT
  // -------------------------------------------------------
  const FFT::Spectrum& fftData = processor->getData();

  size_t fftSize = config->getFFTSize();

  // Check that we have well dimensionated data
  CPPUNIT_ASSERT(fftData.getNbBins() == (fftSize / 2 + 1));
  CPPUNIT_ASSERT(fftData.getType() == COMPLEX_INTERLEAVED);


  // Get PowerSpectrum
  // -------------------------------------------------------
  RMSPowerAveragedSpectrumProcessor* powerProc = new RMSPowerAveragedSpectrumProcessor(ONE_SHOT);

  powerProc->init(1);
  powerProc->process(fftData);

  std::string filename = "Window_";
  switch(type)
  {
  case RECTANGULAR:
    filename += "RECTANGULAR";
    break;
  case BLACKMAN:
    filename += "BLACKMAN";
    break;
  case EXACT_BLACKMAN:
    filename += "EXACT_BLACKMAN";
    break;
  case HAMMING:
    filename += "HAMMING";
    break;
  case HANN:
    filename += "HANN";
    break;
  case FLATTOP:
    filename += "FLATTOP";
    break;
  case BLACKMAN_HARRIS_3:
    filename += "BLACKMAN_HARRIS_3";
    break;
  case BLACKMAN_HARRIS_4:
    filename += "BLACKMAN_HARRIS_4";
    break;
  case BLACKMAN_HARRIS_7:
    filename += "BLACKMAN_HARRIS_7";
    break;
  case LOW_SIDELOBE:
    filename += "LOW_SIDELOBE";
    break;
  }
  filename += ".dat";

  ::saveOutputToFile(filename, powerProc->getData(), 64000);

  delete [] input;
  delete processor;
  delete config;
}

void WindowTest::test_RECTANGULAR()
{
  testWindow(RECTANGULAR);
}

void WindowTest::test_BLACKMAN()
{
  testWindow(BLACKMAN);
}

void WindowTest::test_EXACT_BLACKMAN()
{
  testWindow(EXACT_BLACKMAN);
}

void WindowTest::test_HAMMING()
{
  testWindow(HAMMING);
}

void WindowTest::test_HANN()
{
  testWindow(HANN);
}

void WindowTest::test_FLATTOP()
{
  testWindow(FLATTOP);
}

void WindowTest::test_BLACKMAN_HARRIS_3()
{
  testWindow(BLACKMAN_HARRIS_3 );
}

void WindowTest::test_BLACKMAN_HARRIS_4()
{
  testWindow(BLACKMAN_HARRIS_4);
}

void WindowTest::test_BLACKMAN_HARRIS_7()
{
  testWindow(BLACKMAN_HARRIS_7);
}

void WindowTest::test_LOW_SIDELOBE()
{
  testWindow(LOW_SIDELOBE);
}

