#include "FFTProcessorTest.hpp"

using namespace FFT;

CPPUNIT_TEST_SUITE_REGISTRATION( FFTProcessorTest );

const double PI = 3.1415926535897932;


void FFTProcessorTest::setUp()
{
  printf("\n\n");
  _config = new FFTProcessorConfig();
  _processor = new FFTProcessor();
}

void FFTProcessorTest::tearDown() 
{
  if (_config)
    delete _config;
  if (_processor)
    delete _processor;
}


void FFTProcessorTest::testBasicFFT()
{
  // Initialize
  // -------------------------------------------------------
  unsigned int i = 0;
  double fs = 64000;
  double synthesisFreq = 13527;
  double rb = 10;
  WindowType windowType = HANN;
  double overlap = 25;

  _config->init(fs,rb,windowType,overlap,0);

  _processor->setConfig(*_config);


  // Generate input data
  // -------------------------------------------------------
  size_t blockSize = _config->getWindowSize();
  double* input = createCosine(blockSize, synthesisFreq / fs);
  

  // Send input data
  // -------------------------------------------------------
  bool ready;
  ready = _processor->process(input,blockSize / 2);
  CPPUNIT_ASSERT(ready == false);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == blockSize / 2);

  ready = _processor->process(input + blockSize / 2,blockSize / 2);
  CPPUNIT_ASSERT(ready == true);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == 0);


  // Get FFT
  // -------------------------------------------------------
  const FFT::Spectrum& fftData = _processor->getData();
  size_t fftSize = _config->getFFTSize();

  // Check that we have well dimensionated data
  CPPUNIT_ASSERT(fftData.getNbBins() == (fftSize / 2 + 1));
  CPPUNIT_ASSERT(fftData.getType() == COMPLEX_INTERLEAVED);


  // Extract max module
  // -------------------------------------------------------
  const std::complex<double>* dataValues = fftData.getComplexData();

  FFT::Spectrum moduleData(REAL,
                           fftData.getNbBins(),
                           fftData.getWindowInfo(),
                           fftData.getSamplingFrequency(),
                           fftData.getResolution(),
                           fftData.getFFTSize());

  double *moduleValues = moduleData.getRealData();
  for (i = 0; i < fftData.getNbBins(); i++)
  {
    moduleValues[i] = dataValues[i].real() * dataValues[i].real()
                        + dataValues[i].imag() * dataValues[i].imag();
  }


  double maxValue = 0;
  int maxIndex = 0;

  for (i = 0; i < fftData.getNbBins(); i++)
  {
    if (maxValue < moduleValues[i])
    {
      maxValue = moduleValues[i];
      maxIndex = i;
    }
  }

  // Check that detected peak is ok
  // -------------------------------------------------------
  double resolution = _config->getResolution();
  CPPUNIT_ASSERT( (maxIndex - 1) * resolution < synthesisFreq );
  CPPUNIT_ASSERT( (maxIndex + 1) * resolution > synthesisFreq );


  delete [] input;
}


void FFTProcessorTest::testTwoTimesFFTWithoutOverlap()
{
  // Initialize
  // -------------------------------------------------------
  double fs = 64000.;
  double synthesisFreq = 13527.;
  double rb = 10.;
  WindowType windowType = HANN;
  double overlap = 0.;

  _config->init(fs,rb,windowType,overlap,0);

  _processor->setConfig(*_config);


  // Generate input data
  // -------------------------------------------------------
  size_t blockSize = _config->getWindowSize();
  double* input = createCosine(blockSize, synthesisFreq / fs);


  // Do 1st fft
  // -------------------------------------------------------
  bool ready;
  ready = _processor->process(input,blockSize / 2);
  CPPUNIT_ASSERT(ready == false);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == blockSize / 2);

  ready = _processor->process(input + blockSize / 2,blockSize / 2);
  CPPUNIT_ASSERT(ready == true);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == 0);

  // Get FFT
  const FFT::Spectrum& fftData = _processor->getData();
  size_t fftSize = _config->getFFTSize();

  // Check that we have well dimensionated data
  CPPUNIT_ASSERT(fftData.getNbBins() == (fftSize / 2 + 1));
  CPPUNIT_ASSERT(fftData.getType() == COMPLEX_INTERLEAVED);


  // Do 2nd fft
  // -------------------------------------------------------

  ready = _processor->process(input,blockSize / 4);
  CPPUNIT_ASSERT(ready == false);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == 3 * blockSize / 4);

  ready = _processor->process(input + blockSize / 2, blockSize / 2);
  CPPUNIT_ASSERT(ready == false);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == blockSize / 4);

  ready = _processor->process(input + 3 * blockSize / 4, blockSize / 4);
  CPPUNIT_ASSERT(ready == true);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == 0);

  // Get FFT
  const FFT::Spectrum& fftData2 = _processor->getData();

  // Check that we have well dimensionated data
  CPPUNIT_ASSERT(fftData2.getNbBins() == (fftSize / 2 + 1));
  CPPUNIT_ASSERT(fftData2.getType() == COMPLEX_INTERLEAVED);


  delete [] input;
}

void FFTProcessorTest::testTwoTimesFFTWithOverlap()
{
  // Initialize
  // -------------------------------------------------------
  double fs = 64000.;
  double synthesisFreq = 13527.;
  double rb = 10.;
  WindowType windowType = HANN;
  double overlap = 25.;

  _config->init(fs,rb,windowType,overlap,0);

  _processor->setConfig(*_config);


  // Generate input data
  // -------------------------------------------------------
  size_t blockSize = _config->getWindowSize();
  double* input = createCosine(blockSize, synthesisFreq / fs);

  size_t fftSize = _config->getFFTSize();

  // Do 1st fft
  // -------------------------------------------------------
  bool ready;
  ready = _processor->process(input,blockSize / 2);
  CPPUNIT_ASSERT(ready == false);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == blockSize / 2);

  ready = _processor->process(input + blockSize / 2,blockSize / 2);
  CPPUNIT_ASSERT(ready == true);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == 0);

  // Get FFT
  const FFT::Spectrum& fftData = _processor->getData();

  // Check that we have well dimensionated data
  CPPUNIT_ASSERT(fftData.getNbBins() == (fftSize / 2 + 1));
  CPPUNIT_ASSERT(fftData.getType() == COMPLEX_INTERLEAVED);


  // Do 2nd fft
  // -------------------------------------------------------

  // Here we have 25% of overlap

  ready = _processor->process(input,blockSize / 4);
  CPPUNIT_ASSERT(ready == false);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == blockSize / 2);

  ready = _processor->process(input + blockSize / 2, blockSize / 2);
  CPPUNIT_ASSERT(ready == true);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == 0);

  // Get FFT
  const FFT::Spectrum& fftData2 = _processor->getData();

  // Check that we have well dimensionated data
  CPPUNIT_ASSERT(fftData2.getNbBins() == (fftSize / 2 + 1));
  CPPUNIT_ASSERT(fftData2.getType() == COMPLEX_INTERLEAVED);


  delete [] input;
}


void FFTProcessorTest::testChangeConfig()
{

  // Initialize
  // -------------------------------------------------------
  double fs = 64000.;
  double synthesisFreq = 13527.;

  _config->init(fs, 10., HANN, 0.,0);

  _processor->setConfig(*_config);


  // Generate input data
  // -------------------------------------------------------
  size_t blockSize = _config->getWindowSize();
  double* input = createCosine(blockSize, synthesisFreq / fs);

  size_t fftSize = _config->getFFTSize();

  // Do 1st fft
  // -------------------------------------------------------
  bool ready;
  ready = _processor->process(input,blockSize / 2);
  CPPUNIT_ASSERT(ready == false);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == blockSize / 2);

  ready = _processor->process(input + blockSize / 2,blockSize / 2);
  CPPUNIT_ASSERT(ready == true);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == 0);

  // Get FFT
  const FFT::Spectrum& fftData = _processor->getData();

  // Check that we have well dimensionated data
  CPPUNIT_ASSERT(fftData.getNbBins() == (fftSize / 2 + 1));
  CPPUNIT_ASSERT(fftData.getType() == COMPLEX_INTERLEAVED);


  // Send input for 2nd fft
  // -------------------------------------------------------

  ready = _processor->process(input,blockSize / 4);
  CPPUNIT_ASSERT(ready == false);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == 3 * blockSize / 4);


  // Reconfigure
  // -------------------------------------------------------
  // try changing the resolution bandwidth so as blockSize/fftSize to change
  _config->init(fs, 20., BLACKMAN, 0.,0);
  _processor->setConfig(*_config);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == _config->getWindowSize());

  blockSize = _config->getWindowSize();
  fftSize = _config->getFFTSize();

  // Send input for 2nd fft
  // -------------------------------------------------------

  ready = _processor->process(input,blockSize / 4);
  CPPUNIT_ASSERT(ready == false);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == 3 * blockSize / 4);

  ready = _processor->process(input + blockSize / 2, blockSize / 2);
  CPPUNIT_ASSERT(ready == false);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == blockSize / 4);

  ready = _processor->process(input + 3 * blockSize / 4, blockSize / 4);
  CPPUNIT_ASSERT(ready == true);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == 0);

  // Get FFT
  const FFT::Spectrum& fftData2 = _processor->getData();

  // Check that we have well dimensionated data
  CPPUNIT_ASSERT(fftData2.getNbBins() == (fftSize / 2 + 1));
  CPPUNIT_ASSERT(fftData2.getType() == COMPLEX_INTERLEAVED);


  delete [] input;
}



void FFTProcessorTest::testTooMuchInput()
{
  // Initialize
  // -------------------------------------------------------
  unsigned int i;
  double fs = 64000.;
  double synthesisFreq = 13527.;

  _config->init(fs, 10., HANN, 0.,0);

  _processor->setConfig(*_config);


  // Generate input data
  // -------------------------------------------------------
  size_t blockSize = _config->getWindowSize();
  double* input = createCosine(4 * blockSize, synthesisFreq / fs);

  size_t fftSize = _config->getFFTSize();

  // Test without overlap
  // -------------------------------------------------------
  // Do fft #1
  bool ready;
  ready = _processor->process(input,blockSize);
  CPPUNIT_ASSERT(ready == true);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == 0);

  const FFT::Spectrum* data1 = &_processor->getData();

  size_t n1 = data1->getNbBins();
  std::complex<double>* values1 = new std::complex<double>[n1];
  for (i = 0; i < n1; i++)
    values1[i] = (data1->getRealData())[i];

  _processor->restart();

  // Do fft #2
  ready = _processor->process(input,2 * blockSize);
  CPPUNIT_ASSERT(ready == true);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == 0);

  const FFT::Spectrum* data2 = &_processor->getData();
  size_t n2 = data2->getNbBins();
  std::complex<double>* values2 = new std::complex<double>[n2];
  for (i = 0; i < n2; i++)
    values2[i] = (data2->getRealData())[i];

  // Assert that the 2 computed fft are the same
  CPPUNIT_ASSERT(n1 == n2);
  for (i = 0; i < n1 ; i++)
    CPPUNIT_ASSERT(values1[i] == values2[i]);

  // Test with overlap
  // -------------------------------------------------------
  _processor->reset();
  _config->init(_config->getSamplingFrequency(),
                      _config->getResolution(),
                      _config->getWindow().getWindowType(),
                      25.f,
                      0);
  _processor->setConfig(*_config);

  // blockSize should not have changed, since we only updated overlap
  CPPUNIT_ASSERT(blockSize == _config->getWindowSize());

  // Do fft #1
  ready = _processor->process(input,blockSize);
  CPPUNIT_ASSERT(ready == true);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == 0);

  ready = _processor->process(input + blockSize, blockSize);
  CPPUNIT_ASSERT(ready == true);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == 0);

  data1 = &_processor->getData();


  _processor->restart();

  // Do fft #2
  ready = _processor->process(input,2 * blockSize);
  CPPUNIT_ASSERT(ready == true);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == 0);

  ready = _processor->process(input + blockSize, 2 * blockSize);
  CPPUNIT_ASSERT(ready == true);
  CPPUNIT_ASSERT(_processor->getNbMissingSamples() == 0);

  data2 = &_processor->getData();

  // Assert that the 2 computed fft are the same
  CPPUNIT_ASSERT(data1 == data2);

  delete [] input;
}



void FFTProcessorTest::testReset()
{
}



double* FFTProcessorTest::createCosine(int n, double reducedFrequency)
{
  double* out = new double [n];

  int i;
  for (i = 0; i < n; i++)
    out[i] = cos (2 * PI * reducedFrequency * i);

  return(out);
}


