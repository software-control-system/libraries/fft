#include "VectorAverageTest.hpp"

#include "VectorMagnitudeAveragedSpectrumProcessor.hpp"
#include "VectorPowerAveragedSpectrumProcessor.hpp"

using namespace FFT;


#pragma warning ( disable : 4786 )
CPPUNIT_TEST_SUITE_REGISTRATION( VectorAverageTest );

void VectorAverageTest::testMagnitude_ONE_SHOT() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new VectorMagnitudeAveragedSpectrumProcessor(ONE_SHOT);
  testAverageProcessor(*avProc, ONE_SHOT, "Vector_Magnitude_ONE_SHOT");
  delete avProc;
}

void VectorAverageTest::testMagnitude_AUTO_RESTART() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new VectorMagnitudeAveragedSpectrumProcessor(AUTO_RESTART);
  testAverageProcessor(*avProc, AUTO_RESTART, "Vector_Magnitude_AUTO_RESTART");
  delete avProc;
}

void VectorAverageTest::testMagnitude_MOVING_AVERAGE() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new VectorMagnitudeAveragedSpectrumProcessor(MOVING_AVERAGE);
  testAverageProcessor(*avProc, MOVING_AVERAGE, "Vector_Magnitude_MOVING_AVERAGE");
  delete avProc;
}


void VectorAverageTest::testMagnitude_LINEAR_CONTINUOUS() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new VectorMagnitudeAveragedSpectrumProcessor(LINEAR_CONTINUOUS);
  testAverageProcessor(*avProc, LINEAR_CONTINUOUS, "Vector_Magnitude_LINEAR_CONTINUOUS");
  delete avProc;
}


void VectorAverageTest::testMagnitude_EXPONENTIAL_CONTINUOUS() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new VectorMagnitudeAveragedSpectrumProcessor(EXPONENTIAL_CONTINUOUS);
  testAverageProcessor(*avProc, EXPONENTIAL_CONTINUOUS, "Vector_Magnitude_EXPONENTIAL_CONTINUOUS");
  delete avProc;
}

void VectorAverageTest::testPower_ONE_SHOT() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new VectorPowerAveragedSpectrumProcessor(ONE_SHOT);
  testAverageProcessor(*avProc, ONE_SHOT, "Vector_Power_ONE_SHOT");
  delete avProc;
}

void VectorAverageTest::testPower_AUTO_RESTART() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new VectorPowerAveragedSpectrumProcessor(AUTO_RESTART);
  testAverageProcessor(*avProc, AUTO_RESTART, "Vector_Power_AUTO_RESTART");
  delete avProc;
}

void VectorAverageTest::testPower_MOVING_AVERAGE() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new VectorPowerAveragedSpectrumProcessor(MOVING_AVERAGE);
  testAverageProcessor(*avProc, MOVING_AVERAGE, "Vector_Power_MOVING_AVERAGE");
  delete avProc;
}


void VectorAverageTest::testPower_LINEAR_CONTINUOUS() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new VectorPowerAveragedSpectrumProcessor(LINEAR_CONTINUOUS);
  testAverageProcessor(*avProc, LINEAR_CONTINUOUS, "Vector_Power_LINEAR_CONTINUOUS");
  delete avProc;
}


void VectorAverageTest::testPower_EXPONENTIAL_CONTINUOUS() 
{
  AveragedSpectrumProcessorBase* avProc;
  
  avProc = new VectorPowerAveragedSpectrumProcessor(EXPONENTIAL_CONTINUOUS);
  testAverageProcessor(*avProc, EXPONENTIAL_CONTINUOUS, "Vector_Power_EXPONENTIAL_CONTINUOUS");
  delete avProc;
}
