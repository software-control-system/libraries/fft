#include "UnitConversionTest.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION( UnitConversionTest );

const double PI = 3.1415926535897932;

using namespace FFT;


void UnitConversionTest::setUp()
{
  printf("\n\n");
  _config = new FFTProcessorConfig();
  _fftProcessor = new FFTProcessor();
  _powerProcessor = new RMSPowerAveragedSpectrumProcessor(MOVING_AVERAGE);
  _unitConv = new PowerSpectrumUnitConverter();
}

void UnitConversionTest::tearDown() 
{
  if (_config)
    delete _config;
  if (_fftProcessor)
    delete _fftProcessor;
  if (_powerProcessor)
    delete _powerProcessor;
  if (_unitConv)
    delete _unitConv;
}

double* UnitConversionTest::createInputSignal(int n)
{
  double* out = new double [n];

  int i;
  for (i = 0; i < n; i++)
    out[i] = cos (2 * PI * i);

  return(out);
}


void UnitConversionTest::test_dBV()
{
  int i = 0;

  double fs = 64000;
  double rb = 50;
  WindowType windowType = HANN;
  double overlap = 0.;
  int nbSpectrum = 32;
  int averagingDuration = 4;

  _config->init(fs,rb,windowType,overlap,0);
  _fftProcessor->setConfig(*_config);

  _powerProcessor->init(averagingDuration);
  _unitConv->init(DECIBEL_VOLT_SQUARE, 0, RMS, false, *_config);

  // Generate input data
  // -------------------------------------------------------
  size_t blockSize = _config->getWindowSize();
  double* input = createInputSignal(nbSpectrum * blockSize);
  
  // Send input data
  // -------------------------------------------------------
  bool fftReady;
  bool averageReady;
  for (i = 0; i < nbSpectrum; i++)
  {
    fftReady = _fftProcessor->process(input + i * blockSize, blockSize);
    CPPUNIT_ASSERT(fftReady == true);

    averageReady = _powerProcessor->process(_fftProcessor->getData());

    if (averageReady)
    {
      _unitConv->process(_powerProcessor->getData());

      char tmp[10];
      sprintf(tmp, "%.2d", i);
//      saveOutputToFile(std::string("./output/") + outputFileBaseName + "_" + tmp + ".dat", _powerProcessor->getData(),fs);
    }
  }

  delete [] input;

}

void UnitConversionTest::test_dBmW()
{

}


