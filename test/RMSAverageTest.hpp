#include "AverageTest.hpp"

class RMSAverageTest : public AverageTest
{
  CPPUNIT_TEST_SUITE( RMSAverageTest );
  CPPUNIT_TEST( testMagnitude_ONE_SHOT );
  CPPUNIT_TEST( testMagnitude_AUTO_RESTART );
  CPPUNIT_TEST( testMagnitude_MOVING_AVERAGE );
  CPPUNIT_TEST( testMagnitude_LINEAR_CONTINUOUS );
  CPPUNIT_TEST( testMagnitude_EXPONENTIAL_CONTINUOUS );

  CPPUNIT_TEST( testPower_ONE_SHOT );
  CPPUNIT_TEST( testPower_AUTO_RESTART );
  CPPUNIT_TEST( testPower_MOVING_AVERAGE );
  CPPUNIT_TEST( testPower_LINEAR_CONTINUOUS );
  CPPUNIT_TEST( testPower_EXPONENTIAL_CONTINUOUS );
  CPPUNIT_TEST_SUITE_END();

  public:

    void testMagnitude_ONE_SHOT();
    void testMagnitude_AUTO_RESTART();
    void testMagnitude_MOVING_AVERAGE();
    void testMagnitude_LINEAR_CONTINUOUS();
    void testMagnitude_EXPONENTIAL_CONTINUOUS();

    void testPower_ONE_SHOT();
    void testPower_AUTO_RESTART();
    void testPower_MOVING_AVERAGE();
    void testPower_LINEAR_CONTINUOUS();
    void testPower_EXPONENTIAL_CONTINUOUS();
};
