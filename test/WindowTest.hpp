#include <cppunit/extensions/HelperMacros.h>

#include "WindowInfo.hpp"

class WindowTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE( WindowTest );
  CPPUNIT_TEST( test_RECTANGULAR );
  CPPUNIT_TEST( test_BLACKMAN );
  CPPUNIT_TEST( test_EXACT_BLACKMAN );
  CPPUNIT_TEST( test_HAMMING );
  CPPUNIT_TEST( test_HANN );
  CPPUNIT_TEST( test_FLATTOP );
  CPPUNIT_TEST( test_BLACKMAN_HARRIS_3 );
  CPPUNIT_TEST( test_BLACKMAN_HARRIS_4 );
  CPPUNIT_TEST( test_BLACKMAN_HARRIS_7 );
  CPPUNIT_TEST( test_LOW_SIDELOBE );
  CPPUNIT_TEST_SUITE_END();



  public:

    void setUp();
    void tearDown();

    void test_RECTANGULAR();
    void test_BLACKMAN();
    void test_EXACT_BLACKMAN();
    void test_HAMMING();
    void test_HANN();
    void test_FLATTOP();
    void test_BLACKMAN_HARRIS_3();
    void test_BLACKMAN_HARRIS_4();
    void test_BLACKMAN_HARRIS_7();
    void test_LOW_SIDELOBE();
 
  private:

    void testWindow(FFT::WindowType type);
};

