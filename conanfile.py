from conan import ConanFile

class fftRecipe(ConanFile):
    name = "fft"
    version = "1.0.8"
    package_type = "library"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Library"
    
    license = "GPL-2"
    author = "Nicolas Leclercq"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/libraries/fft"
    description = "FFT library"
    topics = ("utility", "control-system")

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    exports_sources = "CMakeLists.txt", "src/**", "include/**"

    def requirements(self):
        self.requires("yat/[>=1.0]@soleil/stable")
        self.requires("utils/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("fftw/3.3.8@soleil/stable")
        elif self.settings.os == "Windows":
            self.requires("fftw/3.2.2@soleil/stable")
